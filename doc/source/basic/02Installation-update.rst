软件安装与更新
===============

目前最新版本为1.0.0，支持Windows 7/8/10、MacOS、Linux(非arm框架)。


下载软件
-------------

* 【Window系统 地址1】 `Mixly For Win7or10 <http://116.62.49.166/Mixly_WIN.7z>`_ 

* 【Window系统 地址2】 `Mixly For Win7or10 <https://ruilongmaker.cc/download/Mixly_WIN.7z>`_ 

* 【Mac系统】 `Mixly For Mac <https://116.62.49.166/MixlyMac.zip>`_ 


Windows版本安装
-----------------

安装软件
++++++++++++++

下载Mixly_WIN.7z压缩包，右键解压到本地磁盘。

.. note::
	
	- 建议解压到硬盘根目录，路径不能包含中文及特殊字符(如:. _ ( ) 等)。

	- 建议安装路径如E:\Mixly

解压后目录如图所示。

.. image:: images/02Installation-update/mixly_unzip.png

第一次解压的软件只含有最基础的文件，不能直接运行。需要先运行 一键更新.bat 或 update.bat下载最新版的Mixly软件。

等待片刻后，会显示更新进度。

.. image:: images/02Installation-update/update.png

当看到提示“Mixly更新完成”时，说明软件下载完毕。

.. image:: images/02Installation-update/update_finish.png

更新完成后，软件目录如图所示，可以双击Mixly.exe打开Mixly软件。

.. image:: images/02Installation-update/mixly_folder.png

更新软件
++++++++++++++
Mixly软件的更新是基于git设计的，每次更新会自动对比最新版与用户使用的版本的差异，只更新差异文件，避免重复下载造成版本混乱。

先关闭Mixly软件，再运行 一键更新.bat或update.bat 启动更新程序。

.. image:: images/02Installation-update/install5.png

稍等片刻后，更新完成。

.. image:: images/02Installation-update/install6.png

驱动安装
++++++++++++++
开发板与电脑连接通讯需要安装相应的串口芯片驱动，常用的串口芯片驱动有CH340和CP2102。
在arduino/\drivers目录中可以找到这两种串口芯片驱动。

.. image:: images/02Installation-update/install2.png

根据使用的开发板的串口芯片选择相应的驱动，如果不确定是哪种串口芯片，也可以将两个驱动都安装上。

启动软件
++++++++++++++

双击Mixly.exe即可启动Mixly软件。

.. image:: images/02Installation-update/install3.png




Mac版本安装
-----------------

解压软件
++++++++++++++
下载jre-8u221-macosx-x64.dmg Java 安装包，根据提示默认安装。

下载Mixly1.0_MAC.zip压缩包，右键解压到本地磁盘。

解压后目录如图所示。

.. image:: images/02Installation-update/mac.png

1.MAC必须安装JDK8,而且只能安装JDK8,高版本JAVA不行，可以直接使用云盘目录自带的JDK
2.需要安装相应的开源硬件驱动，常用的就是云盘自带的cp210x驱动和ch34x驱动
3.需要自行安装 python3(安装包在云盘)
4.安装完python3之后，运行以下命令行操作

.. code-block:: c
	:linenos:

	sudo xcode-select --install
	ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	brew install portaudio 
	pip3 install pyaudio 
	brew install opencv
	pip3 install opencv-python 
	pip3 install baidu-aip matplotlib pandas numpy 

5.解压Mixy后双击Mixly.jar即可使用

驱动安装
++++++++++++++
开发板与电脑连接通讯需要安装相应的串口芯片驱动，常用的串口芯片驱动有CH340和CP2102。
Mixly/\drivers目录中可以找到这两种串口芯片驱动。

.. image:: images/02Installation-update/driver_mac.png

根据使用的开发板的串口芯片选择相应的驱动，如果不确定是哪种串口芯片，也可以将两个驱动都安装上。

启动软件
++++++++++++++

双击Mixly.jar即可启动Mixly软件。

.. image:: images/02Installation-update/mac.png

更新软件
++++++++++++++
Mixly软件的更新是基于git设计的，每次更新会自动对比最新版与用户使用的版本的差异，只更新差异文件，避免重复下载造成版本混乱。

先关闭Mixly软件。
打开 终端
并通过 cd 命令进入mixly软件所在目录（输入 cd后可以将mixly文件夹拖入 终端 窗口）。

.. image:: images/02Installation-update/mac-terimal.png

依次输入输入更新命令：

.. code-block:: c
	:linenos:

	git add *
	git stash
	git pull origin master

.. image:: images/02Installation-update/mac-update.png

稍等片刻，就可以看到更新文件完成。