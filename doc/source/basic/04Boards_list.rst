支持板卡
==============
从语言上来说，Mixly目前支持Arduino、MicroPython、Python三种语言。
当我们开始使用Mixly编写程序时，需要先在Mixly软件右下角选择正确的板卡

.. image:: images/04Board_list/mixly_board_list.png
 	 :width: 200pt

Arduino板卡
--------------

该类别下采用Arduino编程，支持板卡如下：

* Arduino/Genuino Uno

.. image:: images/04Board_list/uno.jpg
 	 :width: 200pt

* Arduino Nano

.. image:: images/04Board_list/nano.jpg
 	 :width: 200pt

* Arduino/Genuino Mega or Mega 2560

.. image:: images/04Board_list/mega.jpg
 	 :width: 200pt

* Arduino Leonardo

.. image:: images/04Board_list/leonardo.jpg
 	 :width: 200pt

* Generic ESP8266 Module

* NodeMCU 1.0 (ESP-12E Module)

.. image:: images/04Board_list/nodemcu.jpg
 	 :width: 200pt

* LOLIN(WEMOS) D1 R2 & mini

.. image:: images/04Board_list/wemosd1mini.jpg
 	 :width: 200pt

* WeMos D1 R1

.. image:: images/04Board_list/wemosd1.jpg
 	 :width: 200pt

* ESP32 Dev Module

.. image:: images/04Board_list/esp32.jpg
 	 :width: 200pt

* Arduino MixGo
.. image:: images/04Board_list/mixgo.png
 	 :width: 200pt

* Arduino HandBit

.. image:: images/04Board_list/handbit.png
 	 :width: 200pt

* Arduino MixePi

MicroPython板卡
----------------
该类别下采用MicroPython编程，支持板卡如下。

* MicroPython[ESP32_Generic]

.. image:: images/04Board_list/esp32.jpg
 	 :width: 200pt
* MicroPython[ESP32_HandBit]

.. image:: images/04Board_list/handbit.png
 	 :width: 200pt

* MicroPython[ESP32_MixGo]

.. image:: images/04Board_list/mixgo.png
 	 :width: 200pt

* MicroPython[NRF51822_microbit]

.. image:: images/04Board_list/microbit.jpg
 	 :width: 200pt

Python
------------
纯python编程，不需要任何板卡。

* mixpy

