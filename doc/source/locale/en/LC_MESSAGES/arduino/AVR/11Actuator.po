# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2019, Mixly Team
# This file is distributed under the same license as the Mixly Wiki package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Mixly Wiki 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-03 21:29+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.7.0\n"

#: ../../source/Arduino/AVR/11Actuator.rst:2
msgid "执行器"
msgstr ""

#: ../../source/Arduino/AVR/11Actuator.rst:3
msgid "执行模块：声音播放、舵机控制、I2C液晶模块"
msgstr ""

