.. note::
    序列是Python中最基本的数据结构。序列中的每个元素都分配一个数字——它的位置，也称为索引：第一个索引是0，第二个索引是1，依此类推。

    Python有多种内置的序列类型，但最常见的是列表、元组、字典和集合。

    序列都可以进行的操作包括索引，切片，加，乘，检查成员等。

    此外，Python已经内置确定序列的长度以及确定最大和最小的元素等方法。

列表
======================
.. note::
    列表是最常用的Python数据类型，它可以作为一个方括号内的逗号分隔值出现。

    列表的数据项不需要具有相同的数据类型。

    Mixly中有很多用于列表操作的模块，如下：

.. image:: images/06Lists/lists.png

1.列表初始化
-----------------

.. image:: images/06Lists/list_initial.png
.. image:: images/06Lists/list_initial2.png
1.1 描述
++++
    Mixly中有两种初始化列表的方式：

    * 第一种，你需要输入列表名称，将代表各种数据的模块连接到初始化模块上，你也可以点击蓝色齿轮增加元素数量。
    * 第二种，你也需要输入列表名称，然后直接在方括号中输入各种数据，各个元素使用英文逗号分隔即可。

    在使用列表初始化模块后，你可以在"变量"模块分类中根据你输入的列表名称找到代表此列表的模块。

.. attention::
    * 列表的各个元素类型不要求相同，可以是数字、布尔值和字符串，也可以是另一个列表。
    * 列表中的字符串元素都是由引号包围的，若其他数据类型使用引号包围也将会被视为字符串类型。

1.2 示例
++++

    .. image:: images/06Lists/list_initial_example.png

    .. image:: images/06Lists/list_initial_example2.png

    源代码：

    .. code-block:: python
            :linenos:

            mylist = ["Mixly", 1.0, True, [1,2,3,4,5]]
            print(mylist)

    输出结果：

    .. code-block:: python

        ['Mixly', 1.0, True, [1, 2, 3, 4, 5]]

2.列表元素获取
-----------------

2.1根据索引获取元素
++++

.. image:: images/06Lists/list_get_element_by_index.png
2.1.1 描述
****
    此模块可以在列表中根据索引返回对应的元素。

.. attention::
    * 字符串其实也是一种序列，所以列表获取元素和字符串截取字符是同理的。
    * 点击查看 :ref:`array_index`。
    * 在列表中获取单个元素，索引的范围为列表元素数量的相反数到列表元素数量减1，如五个元素的列表索引范围为[-5, 4]，超出此范围的索引会报错。

2.1.2 示例
****

        .. image:: images/06Lists/list_get_element_by_index_example.png

        源代码：

        .. code-block:: python
                :linenos:

                mylist = ["Mixly", "Mixpy", "MixGo", "Mixly Group"]
                print(mylist[0])
                print(mylist[2])

        输出结果：

        .. code-block:: python

            Mixly
            MixGo

2.2 根据索引范围获取多个元素
++++

.. image:: images/06Lists/list_get_elements_by_index.png
2.2.1 描述
****
    此模块可以在列表中根据索引返回对应的多个元素组成的子列表。

.. attention::
    * 所截取出的子列表中，包含前一个索引对应的元素，但不包含后一个索引对应的元素。
    * 点击查看 :ref:`array_index`。
    * 在列表中获取多个元素，索引的范围为列表元素数量的相反数到列表元素数量（因为不包含后一索引对应的元素，所以不用减1），如五个元素的列表索引范围为[-5, 5]，超出此范围的索引会报错。

2.2.2 示例
****

    .. image:: images/06Lists/list_get_elements_by_index_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mylist = ["Mixly", "Mixpy", "MixGo", "Mixly Group"]
            print(mylist[0 : 2])
            print(mylist[2 : 4])

    输出结果：

    .. code-block:: python

        ['Mixly', 'Mixpy']
        ['MixGo', 'Mixly Group']

2.3 随机获取元素
++++

.. image:: images/06Lists/list_get_random_element.png
2.3.1 描述
****
    此模块将返回给定列表中随机的一个元素。

2.3.2 示例
****

    .. image:: images/06Lists/list_get_random_element_example.png

    源代码：

    .. code-block:: python
            :linenos:

            import random
            mylist = ["Mixly", "Mixpy", "MixGo", "Mixly Group"]
            print(random.choice(mylist))
            print(random.choice(mylist))
            print(random.choice(mylist))

    输出结果（每次输出结果是随机的）：

    .. code-block:: python

        MixGo
        Mixly Group
        Mixpy

3.获取列表信息
-----------------

.. image:: images/06Lists/list_get_info.png
3.1 描述
++++
    此模块可以返回一个列表的长度（元素数量）。
    还可以返回数字列表的总和、最大值、最小值、平均数、中位数、众数和标准差。

.. attention::
    * 此模块返回的长度为int类型的数据格式。
    * 此模块只能返回数字列表的总和、最大值、最小值、平均数、中位数、众数和标准差，若列表中包含其他数据类型的元素，则会报错。
    * 此模块返回的平均数和标准差因为经过除法运算，所以返回的是float类型的数据格式。
    * 当给定的数字列表中有多个众数时，将会返回由这多个众数组成的列表。
    * 由于Mixly中此模块使用的math包中不包含计算平均数、中位数、众数和标准差的方法，所以会定义计算这几种数据的方法并调用。

3.2 示例
++++

    .. image:: images/06Lists/list_get_info_example.png

    源代码：

    .. code-block:: python
            :linenos:

            import math
            def math_standard_deviation(numbers):       # 定义计算标准差的方法
                n = len(numbers)
                if n == 0: return
                mean = float(sum(numbers)) / n
                variance = sum((x - mean) ** 2 for x in numbers) / n
                return math.sqrt(variance)
            def math_modes(some_list):      # 定义计算众数的方法
                modes = []
                counts = []
                maxCount = 1
                for item in some_list:
                    found = False
                    for count in counts:
                        if count[0] == item:
                            count[1] += 1
                            maxCount = max(maxCount, count[1])
                            found = True
                    if not found:
                        counts.append([item, 1])
                for counted_item, item_count in counts:
                    if item_count == maxCount:
                        modes.append(counted_item)
                return modes
            def math_median(myList):    # 定义计算中位数的方法
                localList = sorted([e for e in myList if type(e) == int or type(e) == float])
                if not localList: return
                if len(localList) % 2 == 0:
                    return (localList[len(localList) // 2 - 1] + localList[len(localList) // 2]) / 2.0
                else:
                    return localList[(len(localList) - 1) // 2]
            def math_mean(myList):      # 定义计算平均数的方法
                localList = [e for e in myList if type(e) == int or type(e) == float]
                if not localList: return
                return float(sum(localList)) / len(localList)


            num_list = [1,3,5,7,9]
            str_list = ["Mixly", "Mixpy", "MixGo", "Mixly Group"]
            print(len(str_list))
            print(len(num_list))
            print(sum(num_list))
            print(max(num_list))
            print(min(num_list))
            print(math_mean(num_list))
            print(math_median(num_list))
            print(math_modes(num_list))
            print(math_standard_deviation(num_list))


    输出结果：

    .. code-block:: python

        4
        5
        25
        9
        1
        5.0
        5
        [1, 3, 5, 7, 9]
        2.8284271247461903

4.列表元素查询
-----------------

.. image:: images/06Lists/list_search.png
4.1 描述
++++
    此模块可以在给定的列表中查询某个值，选择"位置"将返回第一个匹配元素的索引，选择"个数"将返回匹配元素的数量。

.. attention::
    * 查询的值可以是任意的数据类型
    * 在查询匹配元素的索引时，若列表中无匹配元素，则会报错。
    * 在查询匹配元素的数量时，若列表中无匹配元素，则会返回0。

4.2 示例
++++

    .. image:: images/06Lists/list_search_example.png

    源代码：

    .. code-block:: python
            :linenos:

            num_list = [1,2,2,2,3,3,4,5]
            str_list = ["MixGo", "米思齐", "米思齐", "米思齐", "Mixpy", "北京师范大学"]
            print(num_list.index(3))
            print(num_list.count(2))
            print(str_list.count("米思齐"))

    输出结果：

    .. code-block:: python

        4
        3
        3

5.列表元素赋值
-----------------

.. image:: images/06Lists/list_assign.png
5.1 描述
++++
    此模块可以在给定的列表中根据索引为对应的元素赋值。

.. attention::
    * 此模块没有返回值。
    * 索引必须在列表长度范围内，否则会报错。

5.2 示例
++++

    .. image:: images/06Lists/list_assign_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mylist = [1,2,3,4,5]
            mylist[2] = "Mixly"
            print(mylist)

    输出结果：

    .. code-block:: python

        [1, 2, 'Mixly', 4, 5]

6.列表插入元素
-----------------

.. image:: images/06Lists/list_insert.png
6.1 描述
++++
    此模块可以在给定的列表中给定的索引位置插入元素。

.. attention::
    * 此模块没有返回值。
    * 插入元素之后的所有元素将会依次往后移动一个位置。
    * 若插入时给定的索引超出了原列表的长度，则会默认添加到列表的末尾。
    * 给定的索引可以为负值，规则参考 :ref:`array_index`。

6.2 示例
++++

    .. image:: images/06Lists/list_insert_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mylist = [1,2,3,4,5]
            mylist.insert(1, "目的索引为1")
            mylist.insert(10, "目的索引为10")
            mylist.insert(-3, "目的索引为-3")
            print(mylist)

    输出结果：

    .. code-block:: python

        [1, '目的索引为1', 2, 3, '目的索引为-3', 4, 5, '目的索引为10']

7.列表添加元素
-----------------

.. image:: images/06Lists/list_append_extend.png
7.1 描述
++++
    此模块可以在给定的列表后添加一个或者一组元素。

    "增加"会将添加的对象视为一个元素，"扩展增加"会将添加的对象视为一个序列，将序列的元素分别添加到给定列表的末尾。

.. attention::
    * 此模块没有返回值。
    * 增加使用的是append()方法，会将参数视为一个元素添加到末尾。
    * 扩展增加使用的是extend()方法，参数必须是序列（包括列表、元组、集合、字符串等）对象，如果是非序列对象，则会报错。

7.2 示例
++++

    .. image:: images/06Lists/list_append_extend_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mylist = [1,2,3,4,5]
            mylist.append("Mixly")
            print(mylist)
            mylist = [1,2,3,4,5]
            mylist.extend("Mixly")
            print(mylist)
            mylist = [1,2,3,4,5]
            mylist.append([1,2,3,4,5])
            print(mylist)
            mylist = [1,2,3,4,5]
            mylist.extend([1,2,3,4,5])
            print(mylist)

    输出结果：

    .. code-block:: python

        [1, 2, 3, 4, 5, 'Mixly']        # 在序列后增加"Mixly"
        [1, 2, 3, 4, 5, 'M', 'i', 'x', 'l', 'y']        # 在序列后扩展增加"Mixly"
        [1, 2, 3, 4, 5, [1, 2, 3, 4, 5]]        # 在序列后增加[1, 2, 3, 4, 5]
        [1, 2, 3, 4, 5, 1, 2, 3, 4, 5]      # 在序列后扩展增加[1, 2, 3, 4, 5]

8.列表删除元素
-----------------

.. image:: images/06Lists/list_delete.png
8.1 描述
++++
    此模块可以在给定的列表中删除给定索引位置或者给定值的元素。

.. attention::
    * 此模块没有返回值。
    * 根据给定的值删除元素只会删除列表中匹配的第一个元素。
    * 给定索引位置超出列表长度以及给定值在列表中无匹配元素都会触发报错。
    * 给定的索引可以为负值，规则参考 :ref:`array_index`。

8.2 示例
++++

    .. image:: images/06Lists/list_delete_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mylist = [1,2,3,4,5]
            del mylist[2]
            print(mylist)
            mylist = [1,2,3,4,5]
            mylist.remove(3)
            print(mylist)
            mylist = [1,2,2,2,5]
            mylist.remove(2)
            print(mylist)

    输出结果：

    .. code-block:: python

        [1, 2, 4, 5]
        [1, 2, 4, 5]
        [1, 2, 2, 5]

9.列表删除并获取元素
-----------------

.. image:: images/06Lists/list_pop.png
9.1 描述
++++
    此模块可以在给定的列表中删除给定索引位置的元素，并返回被删除的元素值。

.. attention::
    给定的索引可以为负值，规则参考 :ref:`array_index`。

9.2 示例
++++

    .. image:: images/06Lists/list_pop_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mylist = [1,2,3,4,5]
            print(mylist.pop(-2))
            print(mylist)

    输出结果：

    .. code-block:: python

        4
        [1, 2, 3, 5]

10.列表元素排序
-----------------

.. image:: images/06Lists/list_sequence.png
10.1 描述
++++
    此模块可以将给定的列表按照数字或首字母进行升序或者降序排列，并返回排好序的列表。

.. attention::
    * Mixly中定义了新的方法来对列表进行排序。
    * 此模块并不会改变原序列元素的顺序。
    * 此模块可以将各种数字类型的元素混合排列。
    * 此模块按照首字母区分大小写排列会分别对大写和小写进行给定顺序排列，且小写总在大写后，即升序排列小写在大写后，降序排列小写在大写前。
    * 此模块还可以对同时含有数字和字符串的列表进行排序。若按照数字排序，则只会对列表内的数字排序，字符串会按照原来的顺序，在所有数字后；若按照字母排序，则同时会对数字和字符串进行排序，对字符串的排序规则与上一条相同，且所有字符串在所有数字后。
    * 此模块还可以对多维列表进行排序，由于很少使用，感兴趣的读者可以自行探究，也可以直接探究Mixly中排序方法的定义。

10.2 示例
++++

    .. image:: images/06Lists/list_sequence_example.png

    源代码：

    .. code-block:: python
            :linenos:

            def lists_sort(my_list, type, reverse):
                def try_float(s):
                    try:
                        return float(s)
                    except:
                        return 0
                key_funcs = {
                    "NUMERIC": try_float,
                    "TEXT": str,
                    "IGNORE_CASE": lambda s: str(s).lower()
                }
                key_func = key_funcs[type]
                list_cpy = list(my_list)
                return sorted(list_cpy, key=key_func, reverse=reverse)


            num_list = [1.2,2.5,3,4.3,6.1]
            str_list = ["Aplle", "apple", "Cat", "Dog", "fox"]
            mix_list = ["book", 2.5,1.2,3,5.2,"Door",4.3,"cat"]
            print(lists_sort(num_list, "NUMERIC", True))
            print(num_list)
            print(lists_sort(str_list, "TEXT", False))
            print(lists_sort(str_list, "TEXT", True))
            print(lists_sort(str_list, "IGNORE_CASE", True))
            print(str_list)
            print(lists_sort(mix_list, "NUMERIC", True))
            print(lists_sort(mix_list, "TEXT", False))
            print(lists_sort(mix_list, "IGNORE_CASE", False))
            print(mix_list)

    输出结果：

    .. code-block:: python

        [6.1, 4.3, 3, 2.5, 1.2]     # 按照数字降序排列
        [1.2, 2.5, 3, 4.3, 6.1]     # 原列表
        ['Aplle', 'Cat', 'Dog', 'apple', 'fox']     # 按照首字母升序排列，区分大小写
        ['fox', 'apple', 'Dog', 'Cat', 'Aplle']     # 按照首字母降序排列，区分大小写
        ['fox', 'Dog', 'Cat', 'apple', 'Aplle']     # 按照首字母降序排列，忽略大小写
        ['Aplle', 'apple', 'Cat', 'Dog', 'fox']     # 原列表
        [5.2, 4.3, 3, 2.5, 1.2, 'book', 'Door', 'cat']      # 按照数字降序排列
        [1.2, 2.5, 3, 4.3, 5.2, 'Door', 'book', 'cat']      # 按照首字母升序排列，区分大小写
        [1.2, 2.5, 3, 4.3, 5.2, 'book', 'cat', 'Door']      # 按照首字母升序排列，忽略大小写
        ['book', 2.5, 1.2, 3, 5.2, 'Door', 4.3, 'cat']      # 原列表

11.列表逆序
-----------------

.. image:: images/06Lists/list_reverse.png
11.1 描述
++++
    此模块可以将给定的列表反向。

.. attention::
    * 此模块没有返回值。
    * 此模块会改变原列表的顺序。

11.2 示例
++++

    .. image:: images/06Lists/list_reverse_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mylist = [1,2,3,4,5]
            mylist.reverse()
            print(mylist)
            mylist = ["Mixly", "Mixpy", "MixGo", "Mixly Group"]
            mylist.reverse()
            print(mylist)

    输出结果：

    .. code-block:: python

        [5, 4, 3, 2, 1]
        ['Mixly Group', 'MixGo', 'Mixpy', 'Mixly']

12.列表清空
-----------------

.. image:: images/06Lists/list_clear.png
12.1 描述
++++
    此模块可以清空给定列表的元素。

.. attention::
    * 列表被清空后，只是变成了不包含元素的空列表，此列表对象仍然存在，可以使用列表名调用。
    * 此模块没有返回值。

12.2 示例
++++

    .. image:: images/06Lists/list_clear_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mylist = ["Mixly", "Mixpy", "MixGo", "Mixly Group"]
            mylist.clear()
            print(mylist)

    输出结果：

    .. code-block:: python

        []

13.列表转换
-----------------

.. image:: images/06Lists/list_to_tuple_set.png
13.1 描述
++++
    此模块可以将给定列表转换为元组或集合并返回。

13.2 示例
++++

    .. image:: images/06Lists/list_to_tuple_set_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mylist = ["Mixly", "Mixpy", "MixGo", "Mixly Group"]
            print(tuple(mylist))
            print(set(mylist))

    输出结果：

    .. code-block:: python

        ('Mixly', 'Mixpy', 'MixGo', 'Mixly Group')
        {'Mixly Group', 'Mixpy', 'MixGo', 'Mixly'}      # 因为集合是无序序列，所以每一次输出是随机的


.. _list_zip:

14.列表迭代打包
-----------------

.. image:: images/06Lists/list_zip.png
14.1 描述
++++
    此模块可以将给定的迭代列表中的对应元素打包成一个个元组，然后返回由这一个个元组组成的对象。

.. attention::
    * 如果各个迭代的列表元素数量不一致，则按照元素数量最少的列表进行打包。
    * 此模块返回的是一个对象，需要将此对象转换为想要的序列格式。
    * 若打包迭代的列表数量超过2个，则无法转换为字典，否则会报错。

14.2 示例
++++

    .. image:: images/06Lists/list_zip_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(zip([0,1,2], [0,1,2,3,4]))
            print(list(zip([0,1,2], [0,1,2,3,4])))

    输出结果：

    .. code-block:: python

        <zip object at 0x10c45e580>     # 返回的是一个zip对象
        [(0, 0), (1, 1), (2, 2)]

    示例2：

    .. image:: images/06Lists/list_zip_example2.png

    源代码：

    .. code-block:: python
            :linenos:

            print(zip([0,1,2,3], [0,1,2,3]))
            print(list(zip([0,1,2,3], [0,1,2,3])))
            print(tuple(zip([0,1,2,3], [0,1,2,3])))
            print(dict(zip([0,1,2,3], [0,1,2,3])))
            print(set(zip([0,1,2,3], [0,1,2,3])))
            print(zip(["Name", "Age", "Country"], ["Fred", "19", "China"], ["Tom", "18", "Australia"], ["Sam", "20", "America"]))
            print(list(zip(["Name", "Age", "Country"], ["Fred", "19", "China"], ["Tom", "18", "Australia"], ["Sam", "20", "America"])))
            print(tuple(zip(["Name", "Age", "Country"], ["Fred", "19", "China"], ["Tom", "18", "Australia"], ["Sam", "20", "America"])))
            print(set(zip(["Name", "Age", "Country"], ["Fred", "19", "China"], ["Tom", "18", "Australia"], ["Sam", "20", "America"])))

    输出结果：

    .. code-block:: python

        <zip object at 0x102e92400>     # 返回的是一个zip对象
        [(0, 0), (1, 1), (2, 2), (3, 3)]
        ((0, 0), (1, 1), (2, 2), (3, 3))
        {0: 0, 1: 1, 2: 2, 3: 3}
        {(1, 1), (3, 3), (2, 2), (0, 0)}
        <zip object at 0x102e92400>     # 返回的是一个zip对象
        [('Name', 'Fred', 'Tom', 'Sam'), ('Age', '19', '18', '20'), ('Country', 'China', 'Australia', 'America')]
        (('Name', 'Fred', 'Tom', 'Sam'), ('Age', '19', '18', '20'), ('Country', 'China', 'Australia', 'America'))
        {('Country', 'China', 'Australia', 'America'), ('Age', '19', '18', '20'), ('Name', 'Fred', 'Tom', 'Sam')}


.. _array_index:

附录1：序列索引方向
-------------
.. attention::
    * 有正反两种索引方式。
    * 正向索引中，各个元素的索引从0开始，也就是说第一个元素的索引为0，第九个元素的索引为8，以此类推。
    * 反向索引中，各个元素的索引从列表元素数量的相反数开始，也就是说，对于一个元素数量为5的列表，第一个（倒数第五个）元素的索引为-5，倒数第一个（最后一个）元素的索引为-1，倒数第二个元素的索引为-2，以此类推。