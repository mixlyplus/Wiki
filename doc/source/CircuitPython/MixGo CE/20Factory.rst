自定义模块
===================
Factory中的模块可以方便用户编写Mixly中不支持的传感器功能。

.. image:: images/20Factory/factory.png

1.折叠块请在此描述功能
---------------------

注释说明

.. image:: images/20Factory/1.png

2.import module
---------------------

导入某个用户自定义的模块

.. image:: images/20Factory/import.png

3.from module import *
---------------------

从某个模块中导入全部的方法

.. image:: images/20Factory/from.png

4.callmathod
---------------------

方法调用，无返回值，注意使用时需要传参

.. image:: images/20Factory/call.png

5.callmethod2
---------------------

方法调用，有返回值，注意使用时需要传参

.. image:: images/20Factory/call2.png

6.test
---------------------

类实例化，关于类的更多信息自行网搜

.. image:: images/20Factory/test.png

7.test callmethod 
---------------------

类方法调用，无返回值，需要传参

.. image:: images/20Factory/test_call.png

8.test callmethod 
---------------------

类方法调用，有返回值，需要传参

.. image:: images/20Factory/test_call2.png

9.test.callMethod
---------------------

类方法调用，无返回值，需要传参，用法与7.相似

.. image:: images/20Factory/test3.png

10.mixly
---------------------

变量名自定义

.. image:: images/20Factory/mixly.png

11.print
---------------------

.. image:: images/20Factory/print.png

12.Hello
---------------------

文本代码编写，在该图形块中可非常灵活的进行文本代码编写

.. image:: images/20Factory/Hello.png

暂无范例~