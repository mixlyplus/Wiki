.. note::
    序列是Python中最基本的数据结构。序列中的每个元素都分配一个数字——它的位置，也称为索引：第一个索引是0，第二个索引是1，依此类推。

    Python有多种内置的序列类型，但最常见的是列表、元组、字典和集合。

    序列都可以进行的操作包括索引，切片，加，乘，检查成员等。

    此外，Python已经内置确定序列的长度以及确定最大和最小的元素等方法。

元组
======================
.. note::
    Python的元组与列表类似，不同之处在于元组的元素不能修改。

    元组使用小括号，列表使用方括号。

    Mixly中有很多用于元组操作的模块，如下：

.. image:: images/07Tuple/tuples.png

1.元组初始化
-----------------

.. image:: images/07Tuple/tuple_initial.png
.. image:: images/07Tuple/tuple_initial2.png
1.1 描述
++++
    Mixly中有两种初始化元组的方式：

    * 第一种，你需要输入元组名称，将代表各种数据的模块连接到初始化模块上，你也可以点击蓝色齿轮增加元素数量。
    * 第二种，你也需要输入元组名称，然后直接在小括号中输入各种数据，各个元素使用英文逗号分隔即可。

    在使用元组初始化模块后，你可以在"变量"模块分类中根据你输入的元组名称找到代表此元组的模块。

.. attention::
    * 元组的各个元素类型不要求相同，可以是数字、布尔值和字符串，也可以是另一个元组。
    * 元组中的字符串元素都是由引号包围的，若其他数据类型使用引号包围也将会被视为字符串类型。

1.2 示例
++++

    .. image:: images/07Tuple/tuple_initial_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mytup= ("Mixly", 1.0, True, [1,2,3,4,5], (1,2,3,4,5))
            print(mytup)
            mytup2 = ("Mixly", 1.0, True, [1,2,3,4,5], (1,2,3,4,5))
            print(mytup2)

    输出结果：

    .. code-block:: python

        ('Mixly', 1.0, True, [1, 2, 3, 4, 5], (1, 2, 3, 4, 5))
        ('Mixly', 1.0, True, [1, 2, 3, 4, 5], (1, 2, 3, 4, 5))

2.元组元素获取
-----------------

2.1 根据索引获取元素
++++

.. image:: images/07Tuple/tuple_get_element_by_index.png
2.1.1 描述
****
    此模块可以在元组中根据索引返回对应的元素。

.. attention::
    * 字符串其实也是一种序列，所以元组获取元素和字符串截取字符也是同理的。
    * 点击查看 :ref:`array_index`。
    * 在元组中获取单个元素，索引的范围为元组元素数量的相反数到元组元素数量减1，如五个元素的元组索引范围为[-5, 4]，超出此范围的索引会报错。

2.1.2 示例
****

        .. image:: images/07Tuple/tuple_get_element_by_index_example.png

        源代码：

        .. code-block:: python
                :linenos:

            mytup = ("Mixly", "Mixpy", "MixGo", "Mixly Group")
            print(mytup[0])
            print(mytup[2])

        输出结果：

        .. code-block:: python

            Mixly
            MixGo

2.2 根据索引范围获取多个元素
++++

.. image:: images/07Tuple/tuple_get_elements_by_index.png
2.2.1 描述
****
    此模块可以在元组中根据索引返回对应的多个元素组成的子元组。

.. attention::
    * 所截取出的子元组中，包含前一个索引对应的元素，但不包含后一个索引对应的元素。
    * 点击查看 :ref:`array_index`。
    * 在元组中获取多个元素，索引的范围为元组元素数量的相反数到元组元素数量（因为不包含后一索引对应的元素，所以不用减1），如五个元素的元组索引范围为[-5, 5]，超出此范围的索引会报错。

2.2.2 示例
****

    .. image:: images/07Tuple/tuple_get_elements_by_index_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mytup = ("Mixly", "Mixpy", "MixGo", "Mixly Group")
            print(mytup[0 : 2])
            print(mytup[2 : 4])

    输出结果：

    .. code-block:: python

        ('Mixly', 'Mixpy')
        ('MixGo', 'Mixly Group')

2.3 随机获取元素
++++

.. image:: images/07Tuple/tuple_get_random_element.png
2.3.1 描述
****
    此模块将返回给定元组中随机的一个元素。

2.3.2 示例
****

    .. image:: images/07Tuple/tuple_get_random_element_example.png

    源代码：

    .. code-block:: python
            :linenos:

            import random
            mytup = ("Mixly", "Mixpy", "MixGo", "Mixly Group")
            print(random.choice(mytup))
            print(random.choice(mytup))
            print(random.choice(mytup))

    输出结果（每次输出结果是随机的）：

    .. code-block:: python

        Mixly Group
        MixGo
        Mixpy

3.获取元组信息
-----------------

.. image:: images/07Tuple/tuple_get_info.png
3.1 描述
++++
    此模块可以返回一个元组的长度（元素数量）。
    还可以返回数字元组的总和、最大值、最小值。

.. attention::
    * 此模块返回的长度为int类型的数据格式。
    * 此模块只能返回数字元组的总和、最大值、最小值，若元组中包含其他数据类型的元素，则会报错。

3.2 示例
++++

    .. image:: images/07Tuple/tuple_get_info_example.png

    源代码：

    .. code-block:: python
            :linenos:

            num_tup = (1,3,5,7,9)
            str_tup = ("Mixly", "Mixpy", "MixGo", "Mixly Group")
            print(len(num_tup))
            print(len(str_tup))
            print(max(num_tup))
            print(min(num_tup))
            print(sum(num_tup))


    输出结果：

    .. code-block:: python

        5
        4
        9
        1
        25

4.删除元组
-----------------

.. image:: images/07Tuple/tuple_delete.png
4.1 描述
++++
    此模块可以删除指定的元组。

.. attention::
    * 此模块没有返回值。
    * 删除元组后，无法再使用已被删除的元组名称调用元组，否则会报错。

4.2 示例
++++

    .. image:: images/07Tuple/tuple_delete_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mytup = ("Mixly", "Mixpy", "MixGo", "Mixly Group")
            print(mytup)
            del mytup

    输出结果：

    .. code-block:: python

        ('Mixly', 'Mixpy', 'MixGo', 'Mixly Group')

5.元组连接
-----------------

.. image:: images/07Tuple/tuple_link.png
5.1 描述
++++
    虽然我们无法对元组进行修改，但是我们可以连接元组。

    此模块可以连接两个指定的元组。

.. attention::
    此模块不会对原元组进行修改。

5.2 示例
++++

    .. image:: images/07Tuple/tuple_link_example.png

    源代码：

    .. code-block:: python
            :linenos:

            num_tup = (1,3,5,7,9)
            str_tup = ("Mixly", "Mixpy", "MixGo", "Mixly Group")
            print(num_tup + str_tup + num_tup)
            print(num_tup)
            print(str_tup)

    输出结果：

    .. code-block:: python

        (1, 3, 5, 7, 9, 'Mixly', 'Mixpy', 'MixGo', 'Mixly Group', 1, 3, 5, 7, 9)
        (1, 3, 5, 7, 9)
        ('Mixly', 'Mixpy', 'MixGo', 'Mixly Group')

6.元组转换
-----------------

.. image:: images/07Tuple/tuple_to_list_set.png
6.1 描述
++++
    此模块可以将给定元组转换为列表或集合并返回。

6.2 示例
++++

    .. image:: images/07Tuple/tuple_to_list_set_example.png

    源代码：

    .. code-block:: python
            :linenos:

            str_tup = ("Mixly", "Mixpy", "MixGo", "Mixly Group")
            print(list(str_tup))
            print(set(str_tup))
            print(str_tup)

    输出结果：

    .. code-block:: python

        ['Mixly', 'Mixpy', 'MixGo', 'Mixly Group']
        {'Mixpy', 'MixGo', 'Mixly Group', 'Mixly'}      # 因为集合是无序序列，所以每一次输出是随机的
        ('Mixly', 'Mixpy', 'MixGo', 'Mixly Group')

.. _array_index:

附录1：序列索引方向
-------------
.. attention::
    * 有正反两种索引方式。
    * 正向索引中，各个元素的索引从0开始，也就是说第一个元素的索引为0，第九个元素的索引为8，以此类推。
    * 反向索引中，各个元素的索引从列表元素数量的相反数开始，也就是说，对于一个元素数量为5的列表，第一个（倒数第五个）元素的索引为-5，倒数第一个（最后一个）元素的索引为-1，倒数第二个元素的索引为-2，以此类推。