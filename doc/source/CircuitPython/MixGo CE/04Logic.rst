逻辑
==============
.. note::
    逻辑模块中的指令大多是逻辑运算处理内容，具体可分为：比较运算、范围判断、逻辑运算、代表非、真、假、空的块、？运算、列表存在判断和列表相等判断。

.. image:: images/04Logic/logic.png

1.比较运算符
-----------

.. image:: images/04Logic/compare.png


.. note::
    在Python中，比较运算符是用于比较两个对象的运算符号，结果是一个Boolean逻辑值，为True或者False。


1.1 等于(=)
++++

1.1.1 描述
****
    等于符号用两个对象之间的比较，判断符号两侧的对象的数据值和数据类型是否相等。

1.1.2 规则
****
    如果两个数据值相等，且数据类型相同，则结果为True，否则结果为False。

.. attention::
    * Python中的对象有三个属性：

        1.id:身份标识，是对象在内存中的地址，python中用函数id(x)返回对象 x 在内存中的地址

        2.type:数据类型，python中用函数type(x)返回对象 x 的数据类型

        3.value:值

    * Python中，比较运算符除"=="外还有"is"，他们的区别是：

        "==" 是对两个对象的 value 和 type 进行比较，比较两个对象的值和数据类型是否相等

        "is" 是对两个对象的 id 进行比较 ，比较两个对象的内存地址是否相等

    * Mixly中比较运算符模块中的"="，使用的是Python中的"=="，可以从代码中查看：

        .. image:: images/04Logic/a=b.png
        .. image:: images/04Logic/a==b.png

1.1.3 示例
****

    .. image:: images/04Logic/=_example.png

    返回结果：

    .. code-block:: python

        True
        False
        False

1.2 不等于(≠)
++++
1.2.1 描述
****
    不等于符号使用方法与等于符号相同，返回与等于符号相反的结果。

1.2.2 规则
****
    符号两边的对象值和数据类型相等时，结果False，否则结果为True。

1.2.3 示例
****

    .. image:: images/04Logic/!=_example.png

    返回结果：

    .. code-block:: python

        False
        True
        True

1.3 小于(＜)
++++
1.3.1 描述
****
    小于符号进行大小比较。

1.3.2 规则
****
    如果符号左边的数值小于右边的数值，则返回True，否则返回False。

.. attention::
    在MicroPython中，只有number类型的值（int, float, complex, boolean）可以进行大小比较。

1.3.3 示例
****

    .. image:: images/04Logic/less_than_example.png

    返回结果：

    .. code-block:: python

        False
        True
        False
        True
        False

.. attention::
    在实际的比较中，布尔值True等价于int值1，布尔值False等价于int值0。

1.4 小于等于(≤)
++++
1.4.1 描述
****
    小于等于符号与小于符号类似。

1.4.2 规则
****
    如果符号左边的数值小于等于右边的数值，则返回True，否则返回False。

1.5 大于(>)
++++
1.5.1 描述
****
    大于符号进行大小比较。

1.5.2 规则
****
    如果符号左边的数值大于右边的数值，则返回True，否则返回False。

1.6 大于等于(≥)
++++
1.6.1 描述
****
    大于等于符号与大于符号类似。

1.6.2 规则
****
    如果符号左边的数值大于等于右边的数值，则返回True，否则返回False。

2.范围判断
-----------
.. image:: images/04Logic/range.png

2.1 描述
++++
    如果中间项同时满足左右两个不等式，则返回True，否则返回False。

2.2 示例
++++
    .. image:: images/04Logic/range_example.png

    返回结果：

    .. code-block:: python

        True
        False
        False
        True

3.逻辑运算符
-----------

.. image:: images/04Logic/logic_operators.png

3.1 且
++++
3.1.1 描述
****
    且（and）运算符两边为对象或者逻辑表达式，只有当符号两边的表达式结果均为真时，结果才为真，否则为假。

3.1.2 示例
****

    .. image:: images/04Logic/and_example.png

    返回结果：

    .. code-block:: python

        True
        False

3.2 或
++++
3.2.1 描述
****
    或（or）运算符两边为对象或者逻辑表达式，只有当符号两边的表达式结果均为假时，结果才为为假，否则为真。

3.2.2 示例
****

    .. image:: images/04Logic/or_example.png

    返回结果：

    .. code-block:: python

        True
        False

4.否定运算符
----
.. image:: images/04Logic/not.png

4.1 描述
++++
    否定运算符（非）(not)用于对象或表达式前面，结果与原对象或表达式逻辑值相反。即原逻辑值为True，则结果为False；若原逻辑值为False，则结果为True。

4.2 示例
++++
    .. image:: images/04Logic/not_example.png

    返回结果：

    .. code-block:: python

        True
        True
        False

5.逻辑值
----
.. image:: images/04Logic/true_false.png

5.1 描述
++++
    MicroPython中的逻辑值包含真(True)，假(False)。逻辑值可以直接参与逻辑运算，得出的运算结果也是逻辑值。

5.2 示例
++++

    .. image:: images/04Logic/true_false_example.png

    返回结果：

    .. code-block:: python

        False
        True
        False

6.空值(None)
----
.. image:: images/04Logic/none.png

6.1 描述
++++
    * None是一个特殊的常量。
    * None和False不同。
    * None不是0。
    * None不是空字符串。
    * None和任何其他的数据类型比较永远返回False。

6.2 示例
++++

    .. image:: images/04Logic/none_example.png

    返回结果：

    .. code-block:: python

        True
        False
        False
        False
        False
        False

7. ？：模块
----

.. image:: images/04Logic/if_operator.png

7.1 描述
++++
    对于条件表达式模块 a ? x : y，先计算条件 a 的逻辑结果，然后进行判断。
    如果 a 的值为True，则计算表达式 x 的值，模块的值即为 x 的值；
    否则，计算表达式 y 的值，模块的值即为 y 的值。

7.2 参数
++++
    * a: 判断条件，先计算判断条件的值
    * x: 当判断条件的值为True，模块的值即为x的值
    * y: 当判断条件的值为False，模块的值即为y的值

.. hint::
    * 一个条件表达式不会既计算 x ，又计算 y 。
    * 条件运算符是右结合的，也就是说，从右向左分组计算。例如，a ? b : c ? d : e 将按 a ? b : (c ? d : e) 执行。
    .. image:: images/04Logic/if_operator_block1.png
    * 实际上，MicroPython中没有 ? : 语句，这实际上是通过 if 条件语句实现的。
    .. image:: images/04Logic/if_operator_block.png
    .. image:: images/04Logic/if_operator_code.png

7.3 示例
++++
    这一段代码可以在 a 和 b 中选择出较大数字。

    .. image:: images/04Logic/if_operator_example.png

    返回结果：

    .. code-block:: python

        5

8.元素存在判断
----
.. image:: images/04Logic/in_list.png

8.1 参数
++++
        元素：可以是各种常量和变量对象，包括数列。

        数列：需要被判断是否存在某元素的数列；是多维数列时要求前一参数是对应维度的数列。

    此模块的值为逻辑值，当参数元素存在于数列中时，此模块值为True；当参数元素不存在于数列中时，此模块值为False。

8.2 示例
++++

    .. image:: images/04Logic/in_list_example.png

    返回结果：

    .. code-block:: python

        True
        False

9.数列相等判断
----
.. image:: images/04Logic/list_equal.png

9.1 描述
++++
    此模块的两个参数均为数列对象，模块的值为逻辑值，当前后两数列相等时，此模块值为True；当前后两数列不相等时，此模块值为False。

.. attention::
    * 此模块使用的是"is"进行相等判断。还记得之前讲过的Python对象的三个属性和"=="与"is"判断的区别吗？

    * Python中的对象有三个属性：

        1.id:身份标识，是对象在内存中的地址，python中用函数id(x)返回对象 x 在内存中的地址

        2.type:数据类型，python中用函数type(x)返回对象 x 的数据类型

        3.value:值

    * Python中，比较运算符除"=="外还有"is"，他们的区别是：

        "==" 是对两个对象的 value 和 type 进行比较，比较两个对象的值和数据类型是否相等

        "is" 是对两个对象的 id 进行比较 ，比较两个对象的内存地址是否相等

    * 所以此模块判断的也是判断前后两个数列的内存地址是否相等，请结合一下示例进一步了解。

9.2 示例
++++

    .. image:: images/04Logic/list_equal_example.png

    返回结果：

    .. code-block:: python

        False
        True
