MixIO
=================

MQTT是一个基于客户端-服务器的消息发布/订阅传输协议。MQTT协议是轻量、简单、开放和易于实现的，这些特点使它适用范围非常广泛。在很多情况下，包括受限的环境中，如：机器与机器（M2M）通信和物联网（IoT）。更多信息自行网搜。
Mixio中包含了众多MQTT操作指令，其中较为常用的有：wifi连接、使用Mixly Key连接至MixIO平台、使用授权码连接至MixIO平台、创建MQTT客户端并连接、订阅主题、发送消息、循环接收订阅、函数方法等等。

.. image:: ../images/19IOT/Mixio/mixio.png

1. 连接WIFI
-----------------------

.. image:: ../images/19IOT/Mixio/wifi.png

该指令用于连接无线网络，其中参数名称为无线网账号，密码为无线网密码，该条指令的正常执行是物联网编程的重要编程步骤之一。

* 注意wifi账号密码尽量简洁，以英文数字为主,同时避免使用需要二次验证的网络和5G网络。

2. 三种方式连接到 MixIO平台
-------------------------------

.. image:: ../images/19IOT/Mixio/loging.png

推荐使用谷歌浏览器打开该网址：mixio.mixly.org


2.1使用 Mixly Key
++++++++++++++++++++++++

.. image:: ../images/19IOT/Mixio/mixlykey.png

每台安装Mixly软件的电脑会有独立的Mixly Key，借助Mixly Key即可无需注册账号快速登录MixIO平台。

* 这里需要注意的是，Mixly Key登录后对应的是独立的单物联网项目工程，与2.3账号密码登录相区别。

2.2使用项目授权码
++++++++++++++++++++++++

.. image:: ../images/19IOT/Mixio/a1b2c3.png

使用授权码登录，避免从零开始搭建物联网云端组件，有利于提升教学效率。
授权码的获取分以下两个步骤：①在2.3账号密码登录的情况下，②点击新建项目

2.3使用账号密码
++++++++++++++++++++++++

.. image:: ../images/19IOT/Mixio/mclient.png
* 参数1服务器地址：mixio.mixly.org 无需修改，复制该网址至浏览器打开即可
* 参数2代理网口：1883 无需修改
* 参数3用户名：需要至上述网址用邮箱注册一个账号填到这里
* 参数4密码：此处密码非注册账号时所填写的密码，需要在以账号密码登录后至下图左侧①箭头处，点击小眼睛获取
* 参数5项目：项目名称需要根据实际情况，填入在以账号密码登录后至下图②箭头处，新建项目时自定义的名称

.. image:: ../images/19IOT/Mixio/password.png

* 账号密码登录可创建多个不同的物联网项目工程，同时还支持项目工程的共享及导入等操作。

3. MQTT发送数据
-------------------------------

.. image:: ../images/19IOT/Mixio/send1.png

该指令用于将字符串数据上传至物联网云端，主要包含两个参数，分别为主题和消息，其中主题一般推荐用英文命名，消息内容需自行转化为字符串以免出错，上传至云端后查看方式，可以在数据视图下（组件视图见后面范例），点击左上角监听主题进行切换查看上传内容。

* 该指令只能在wifi连接和mixio平台连接成功之后正常运行。

关于主题更多信息，参见：https://blog.csdn.net/weixin_43025071/article/details/82464661

.. image:: ../images/19IOT/Mixio/data.png

4. MQTT订阅主题
-------------------------------

.. image:: ../images/19IOT/Mixio/method.png

该指令用于获取物联网云端下发的数据，主要包含两个参数，分别为主题和消息，其中主题填入物联网端发送数据所定义的名称，设置回调函数见5.，“method”需根据具体情况自定义，当所订阅的主题有数据下发时，会调用该函数执行相关功能。

5. MQTT回调函数
-------------------------------

.. image:: ../images/19IOT/Mixio/method2.png

该方法函数每次被调用都会传过来三个参数，分别对应于client(客户端)、topic(主题)、msg(下发的内容，字符串格式)，在变量区块可找到对应变量名图形代码块。关于函数及其参数相关概念，自行网搜了解。

6. MQTT循环接收订阅超时
-------------------------------

.. image:: ../images/19IOT/Mixio/wait.png

若想让4.中订阅的主题内容可以正常调用回调函数，就需在编写的程序主循环中加入这句指令，每次执行该句指令都会询查所订阅的主题是否有数据发送过来。

* 超时1秒，会在这里耗时较长导致程序运行卡顿，通常改为0.3秒较为适宜。

7. 范例1——Mixly Key的使用
---------------------------
通过Mixly Key连接至MixIO平台，每隔1秒上传随机数至MixIO平台，通过数据视图查看。

7.1 Mixly 软件端图形代码：
++++++++++++++++++++++++++

.. image:: ../images/19IOT/Mixio/m_1.png

Python代码略。

浏览器端使用Mixly Key登录(网址：mixio.mixly.org)

* 推荐使用谷歌浏览器，其他浏览器可能存在中文兼容性问题！

.. image:: ../images/19IOT/Mixio/key.png

7.2 浏览器端数据效果：
++++++++++++++++++++++++

.. image:: ../images/19IOT/Mixio/ex_l1.png

* 注意①点击数据视图，②在MixGo CE端代码正常运行且点阵屏显示了Mok，基本上浏览器②箭头处会显示有设备连接平台，③选择对应的主题即可看到上传的数据。

.. Attention::
    上述范例中，需要注意上传的数据格式为字符串格式，同时注意加延时，否则浏览器端会出现响应卡顿现象。

8. 范例2——Mixly账号密码登录
---------------------------

.. image:: ../images/19IOT/Mixio/ps2.png

根据2.3中步骤，登录mixio平台后，将账号及平台生成的密码填至MQTT创建客户端指令中相应位置，并点击Mixio平台的右上角新建一个工程项目，将名称填至项目处。

.. image:: ../images/19IOT/Mixio/re_2.png

下面将通过在数据页面定义一个主题下发数据至MixGo CE端，CE端将收到的数据打印出来。

8.1 Mixly 软件端图形代码：
++++++++++++++++++++++++++

.. image:: ../images/19IOT/Mixio/re_1.png

Python代码略。

当MixGo CE点阵屏上显示MOK，即表示wifi和云端均正常连接，若未正常出现MOK，自行检查wifi信息、mixio平台账号、密码及项目名等，所填信息避免出现空格。

.. image:: ../images/19IOT/Mixio/re_3.png

正常情况下，云端发送数据后，本地mixly软件端即可看到打印出的数据内容。

.. Attention::
    上述范例中，需要注意收到的数据格式为字符串格式，在做逻辑判断时尤其需要注意。

* 根据以上两个示例可知，物联网基础编程步骤大致为：①连接平台、②数据上传、③数据下发，关于云端组件和逻辑的使用案例见以往培训视频及PPT。

9. 使用授权码的登录方式待完善
----------------------------

10. 格式化位置信息
----------------------------

.. image:: ../images/19IOT/Mixio/location.png

该指令可以用于组件中地图组件位置标定，例如，圆明园的坐标大致为（116.3100，40.0156），通过该指令进行定位并标记“圆明园”。因此三个参数分别对应为“116.3100、40.0156、圆明园”

10.1 组件使用范例
++++++++++++++++++++++++

组件的使用大致分四步，先点击组件按钮、 然后点击加号、之后点击数据地图加号，注意此时会出现新的弹窗，组件名称可命名为地图定位，消息主题这里直接默认为“map”不修改，点击对号确认，最后点击右上角的三角按钮运行该工程！！！！这点很重要，否则看不到效果。

.. image:: ../images/19IOT/Mixio/location1.png

10.2新建组件后效果
++++++++++++++++++++++++

.. image:: ../images/19IOT/Mixio/location3.png

10.3程序示例
++++++++++++++++++++++++

.. image:: ../images/19IOT/Mixio/location2.png

10.4程序上传后，云端定位效果
++++++++++++++++++++++++++++++

.. image:: ../images/19IOT/Mixio/location_result.png


11. 格式化字符串JSON
----------------------------

.. image:: ../images/19IOT/Mixio/json.png

将字符串格式化为json格式，然后进行上传至云端，可通过串口直接打印该指令格式后的内容，关于json数据格式内容见网络。

* 以下指令增加了物联网编程的灵活性，学有余力可多多尝试。

.. image:: ../images/19IOT/Mixio/reconnect.png

.. image:: ../images/19IOT/Mixio/connect_close.png

.. image:: ../images/19IOT/Mixio/client_del.png

.. image:: ../images/19IOT/Mixio/is_connect.png

.. image:: ../images/19IOT/Mixio/mqtt_server.png

.. image:: ../images/19IOT/Mixio/log.png

.. image:: ../images/19IOT/Mixio/topic_cancel.png

.. image:: ../images/19IOT/Mixio/method_del.png

.. image:: ../images/19IOT/Mixio/name_pass.png

.. image:: ../images/19IOT/Mixio/mqtt_topic_send.png

.. image:: ../images/19IOT/Mixio/topic_method.png
