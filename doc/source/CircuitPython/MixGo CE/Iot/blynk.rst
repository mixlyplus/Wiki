Blynk IOT
=================
Blynk是一项物联网（IoT）服务，旨在使远程控制和从您的设备中读取传感器数据的速度尽可能快和容易。

.. image:: ../images/19IOT/Blynk/blynk.png

1.Blynk准备工作
-----------------

1.1下载blynk APP
+++++++++++++++++
若手机为安卓系统，通过浏览器扫描下图二维码下载app（不要通过微信扫描！），苹果手机通过其商店搜索下载即可。

.. image:: ../images/19IOT/Blynk/down.png

1.2 blynk账号注册
+++++++++++++++++
按下图5个步骤注册账号，尤其注意切换使用blynk.mixly.org服务器，否则将出现访问缓慢问题。

.. image:: ../images/19IOT/Blynk/blynk_log.png

1.3 blynk账号登录
+++++++++++++++++
按下图2个步骤登录账号

.. image:: ../images/19IOT/Blynk/blynk_log2.png

1.4 创建项目并获取授权码
+++++++++++++++++++++++++++
按下图根据下面步骤完成项目创建并将复制到的授权码发至电脑端。

.. image:: ../images/19IOT/Blynk/project.png

2.blynk编程设置
-----------------
在完成1.中相关操作后，现在就可以通过Mixly软件进行编程，在下面这条指令三个参数中，前两个无需修改，仅需将1.4中获取的授权码放至第三个参数位置即可。

.. image:: ../images/19IOT/Blynk/set_init.png

3.当从Blynk app收到虚拟引脚数据时触发该指令
---------------------------------------------------
该指令用于获取从Blynk app发送过来的虚拟模拟量数值，其中有两个参数，0处具体数值需根据APP端选定的管脚号进行设置；"value"为传参变量名可自定义，定义后出现在变量区块中。
* 注意该指令需要将6.中指令放在循环中运行方可触发，具体使用见后面范例。

.. image:: ../images/19IOT/Blynk/get_pin_value.png

4.Blynk定时器
---------------------
该指令通过硬件自带的定时器模提供执行多进程任务的功能，其中有三个参数可设置，0处为定时器名称，可默认不变；“多次触发”选项意指一旦该定时器功能启动将会执行多次所包含指令，当选择单次触发时，仅执行一次包含的指令；周期意指每隔多久执行一次该指令相应功能。
* 注意此指令需要将7中指令放在循环中运行方可触发，具体使用见后面范例。

.. image:: ../images/19IOT/Blynk/timer.png

5.查询可用的Blynk定时器
-------------------------
该指令配合6、7中指令使用，返回{'0_timer': 'Running'}或{'0_timer': 'Stopped'}。

.. image:: ../images/19IOT/Blynk/is_useable_timer.png

6.停止Blynk定时器
-------------------------
该指令用于停止某个正在运行的定时器。

.. image:: ../images/19IOT/Blynk/stop_timer.png

7.运行Blynk进程
-------------------------
该指令放于主循环中，可使2中指令得以运行。

.. image:: ../images/19IOT/Blynk/run_process.png

7.1范例
++++++++
运行blynk进程，当blynk app端虚拟引脚数据改变时，CE端接收并通过串口打印数据。
* Blynk APP端添加Slider组件并选择虚拟引脚V0。

.. image:: ../images/19IOT/Blynk/example_11.png

.. image:: ../images/19IOT/Blynk/example1.png

8.运行Blynk定时器进程
-------------------------
该指令放于主循环中，可使3中指令得以运行。

.. image:: ../images/19IOT/Blynk/run_timer_process.png

8.1范例
++++++++
运行blynk定时器进程，选择多次触发，每隔2秒触发一次串口打印输出“from timer”。

.. image:: ../images/19IOT/Blynk/example2.png

9.发送数据到Blynk App虚拟引脚
-----------------------------
该指令用于控制Blynk app端虚拟引脚数据的改变。

.. image:: ../images/19IOT/Blynk/send_data.png

9.1.范例
+++++++++++
通过定时器进程、全局变量、如果判断实现，每隔1秒改变一次Blynk app Gauge组件的显示状态。
准备工作：按下图在App端添加Gauge组件，并选择设置V1引脚。

.. image:: ../images/19IOT/Blynk/example_22.png

.. image:: ../images/19IOT/Blynk/example91.png

9.2程序执行效果
++++++++++++++++++

.. image:: ../images/19IOT/Blynk/example911.png

10.Blynk App显示通知
-----------------------------
该指令用于向app端发送通知，目前暂不可用。

.. image:: ../images/19IOT/Blynk/display_in.png

11.当Blynk连接时
-----------------------------
当2中blynk进程运行时，该指令将被触发执行。

.. image:: ../images/19IOT/Blynk/connect.png

12.当Blynk断开时
-----------------------------
当2中blynk进程停止运行时，该指令将被触发执行。

.. image:: ../images/19IOT/Blynk/break_connect.png

13.同步虚拟管脚的状态
-----------------------------
该指令用于同步某一虚拟引脚的状态

.. image:: ../images/19IOT/Blynk/staus.png

例如在9.1范例基础上，同步V1引脚状态并通过串口打印输出其状态值。
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. image:: ../images/19IOT/Blynk/example3.png


14.邮件发送
-----------------------------
向某个邮箱发送邮件信息，该指令待进一步测试完善。

.. image:: ../images/19IOT/Blynk/subject_send.png

15.给管脚添加属性
-----------------------------
该指令用于设置虚拟管脚属性，目前暂不可用。

.. image:: ../images/19IOT/Blynk/add.png
