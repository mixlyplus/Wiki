文本
===============
.. note::
    文本分类下的模块主要包含文本输出、文本处理和字符串内容检索等功能。具体包括：文本连接、文本转换、获取长度、格式化字符串等。

    所有文本内容都可通过串口打印或外接显示屏输出。

.. image:: images/05Text/text1.png
.. image:: images/05Text/text2.png
.. image:: images/05Text/text3.png

1.字符串
------------

.. image:: images/05Text/string.png
1.1 描述
++++
    Mixly中包含单行文本和多行文本。
    单行文本内容可以是一个字符、单词也可以是一行文本。
    多行文本内容会保留输入的换行。

1.2 示例
++++
    Python中的单行文本由双引号或者单引号包围，多行文本由连续使用的三个单引号包围。

    .. code-block:: python
        :linenos:

        "Mixly"
        '''Hello
        Mixly'''

2.字符串连接
------------

.. image:: images/05Text/string_link.png
2.1 描述
++++
    字符串连接模块可以连接单行文本和多行文本，也可以嵌套使用。此模块在连接的文本之间不会添加任何分隔符号。

2.2 示例
++++
    你也可以在单行文本或者多行文本中使用转义字符 "\\n" 来进行换行。

    .. image:: images/05Text/string_link_example.png

    源代码：

    .. code-block:: python
        :linenos:

        print("Hello" + "Mixly\n")
        print("Hello\n" + "Mixly\n")
        print("Hello" + "Mixly" + "Love" + "Mixly\n")
        print("Hello" + '''Mixly
        1.0\n''')
        print('''Hello
        World''' + "Mixly\n")
        print('''Hello
        World\n''' + "Mixly\n")

    输出结果：

    .. code-block:: python

        HelloMixly

        Hello
        Mixly

        HelloMixlyLoveMixly

        HelloMixly
        1.0

        Hello
        WorldMixly

        Hello
        World
        Mixly

3.字符串转换
------------

3.1 字符串转换为其他类型
++++

.. image:: images/05Text/string_transform.png
3.1.1 描述
****
    此模块可以将字符串转换为小数、整数和字节。转为字节实际上是使用utf-8来对字符串进行编码。

.. attention::
        如果字符串的内容不是数字，则使用此模块将其转换为数字会报错。

3.1.2 示例
****

        .. image:: images/05Text/string_transform_example.png

        源代码：

        .. code-block:: python
                :linenos:

                a = "123"
                b = "1.23"
                c = "Mixly"
                print(int(a))
                print(float(b))
                print(a.encode("utf-8"))
                print(b.encode("utf-8"))
                print(c.encode("utf-8"))

        输出结果：

        .. code-block:: python

            123
            1.23
            b'123'
            b'1.23'
            b'Mixly'

3.2 数字转换为字符串
++++

.. image:: images/05Text/transform_string.png
3.2.1 描述
****
    此模块可以将数字转换为字符串。

3.2.2 示例
****

    .. image:: images/05Text/transform_string_example.png

    源代码：

    .. code-block:: python
            :linenos:

            a = 123
            b = 1.23
            print(str(a))
            print(str(b))
            print(str(100))

    输出结果：

    .. code-block:: python

        123
        1.23
        100

3.3 转ASCII字符
++++

.. image:: images/05Text/ascii_transform.png
3.3.1 描述
****
    此模块可以返回一个ASCII编码对应的文本符号。

3.3.2 示例
****

    .. image:: images/05Text/ascii_transform_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(chr(38))
            print(chr(65))

    输出结果：

    .. code-block:: python

        &
        A

.. hint::
    ASCII码表:

    .. image:: images/05Text/ascii-Table.png
    .. image:: images/05Text/ascii-Table2.jpg

3.4 转ASCII数值
++++

.. image:: images/05Text/transform_ascii.png
3.4.1 描述
****
    此模块可以返回一个字符对应的ASCII编码。

3.4.2 示例
****

    .. image:: images/05Text/transform_ascii_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(ord("&"))
            print(ord("A"))

    输出结果：

    .. code-block:: python

        38
        65

4.获取字符串长度
-----------------

.. image:: images/05Text/string_length.png
4.1 描述
++++
    此模块可以返回一个字符串的长度或者列表、元组、集合的元素数量。对于字符串，一个英文字母、一个符号（比如空格）、一个汉字长度均为1。

4.2 返回值
++++
    此模块返回的长度为int类型的数据格式。

4.3 示例
++++

    .. image:: images/05Text/string_length_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(len("Mixly"))
            print(len("北京师范大学米思齐团队"))
            print(len("I Love Mixly!"))
            print(len([1,2,3]))
            print(len((1,2,3,4)))
            print(len({1,2,3,4,5}))

    输出结果：

    .. code-block:: python

        5
        11
        13
        3
        4
        5

5.字符串判断
-----------------

.. image:: images/05Text/string_judgement.png
5.1 描述
++++
    此模块可以判断前后两个字符串是否相等，也可以判断某字符串是否以某字符或字符串开始或结尾。若相等或以指定字符串开始或结尾，则返回True，否则返回False。

.. attention::
    * 此模块的判断都是严格区分大小写的。
    * 可以判断某字符串是否以另一字符串开始或结尾。

5.2 示例
++++

    .. image:: images/05Text/string_judgement_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print("Mixly" == "Mixly")
            print("Mixly" == "mixly")
            print("米思齐" == "米思皮")
            print("Mixly".startswith("m"))
            print("Mixly".endswith("y"))
            print("Mixly".endswith("ly"))

    输出结果：

    .. code-block:: python

        True
        False
        False
        False
        True
        True

6.字符串截取
------------

6.1 根据索引截取字符
++++

.. image:: images/05Text/string_get_char.png
6.1.1 描述
****
    此模块可以在字符串中根据索引返回对应的字符。

.. attention::
    * 点击了解 :ref:`string_index`。
    * 在字符串中截取字符，索引的范围为字符串长度的相反数到字符串的长度减1，如五个字符的字符串索引范围为[-5, 4]，超出此范围的索引会报错。

6.1.2 示例
****

        .. image:: images/05Text/string_get_char_example.png

        源代码：

        .. code-block:: python
                :linenos:

                print("Mixly"[0])
                print("Mixly"[2])
                print("Mixly"[4])
                print("Mixly"[(-5)])
                print("Mixly"[(-3)])
                print("Mixly"[(-1)])

        输出结果：

        .. code-block:: python

            M
            x
            y
            M
            x
            y

6.2 根据索引范围截取字符串
++++

.. image:: images/05Text/string_get_string.png
6.2.1 描述
****
    此模块可以在字符串中根据索引返回对应的字符串。

.. attention::
    * 所截取出的字符串中，包含前一个索引对应的字符，但不包含后一个索引对应的字符。
    * 点击了解 :ref:`string_index`。
    * 在字符串中截取字符串，索引的范围为字符串长度的相反数到字符串的长度（因为不包含后一索引对应的字符，所以不用减1），如五个字符的字符串索引范围为[-5, 5]，超出此范围的索引会报错。

6.2.2 示例
****

    .. image:: images/05Text/string_get_string_example.png

    源代码：

    .. code-block:: python
            :linenos:

            str = "北京师范大学米思齐团队"
            print(str[0 : 6])       #截取第一个到第六个字符
            print(str[-5 : -1])     #截取倒数第五个到倒数第二个字符

    输出结果：

    .. code-block:: python

        北京师范大学
        米思齐团

6.3 截取随机字符
++++

.. image:: images/05Text/string_get_random_char.png
6.3.1 描述
****
    此模块将返回给定字符串中随机的一个字符。

6.3.2 示例
****

    .. image:: images/05Text/string_get_random_char_example.png

    源代码：

    .. code-block:: python
            :linenos:

            import random
            print(random.choice("Mixly"))
            print(random.choice("Mixly"))
            print(random.choice("Mixly"))

    输出结果（每次输出结果是随机的）：

    .. code-block:: python

        l
        M
        y

7.字符串编/解码
-----------------

7.1 描述
++++
    此模块可以对指定字符串进行指定类型的编码和解码，并返回编码和解码的结果。

.. attention::
    使用错误的类型进行解码可能会获得乱码，或者报错。

7.2 示例
++++

    .. image:: images/05Text/string_decode_encode_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print("Mixly".encode("ASCII"))
            print("Mixly".encode("ASCII").decode("ASCII"))
            print("米思齐".encode("utf-8"))
            print("米思齐".encode("utf-8").decode("utf-8"))
            print("米思齐".encode("gbk"))
            print("米思齐".encode("gbk").decode("gbk"))

    输出结果：

    .. code-block:: python

        b'Mixly'
        Mixly
        b'\xe7\xb1\xb3\xe6\x80\x9d\xe9\xbd\x90'
        米思齐
        b'\xc3\xd7\xcb\xbc\xc6\xeb'
        米思齐

.. hint::
    了解更多关于字符编码的知识：

    * `字符编码-百度百科 <https://baike.baidu.com/item/%E5%AD%97%E7%AC%A6%E7%BC%96%E7%A0%81/8446880>`_
    * `常用编码格式介绍 <https://www.cnblogs.com/xiaojidanbai/p/10826472.html>`_

8.字符串内容转换
-----------------

.. image:: images/05Text/string_switch.png
8.1 描述
++++
    此模块可以对指定字符串进行指定类型的转换。转换类型包括将各单词首字母转换为大写，将句子首字母转换为大写，将字符串中所有字母的大小写对调，以及将所有字母转换为小写字母。

.. attention::
    * 将各单词首字母转换为大写功能需要各个单词间有分隔符（空格，逗号均可），若没有分隔符则视为同一个单词。
    * 将句子首字母转换为大写实际上是将整个字符串第一个字母转换为大写，后面的句子并不会转换。

8.2 示例
++++

    .. image:: images/05Text/string_switch_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print("hello mixly!".title())
            print("hello mixly! i love mixly!".capitalize())
            print("hello mixly!".title().swapcase())
            print("hello mixly!".title().swapcase().lower())

    输出结果：

    .. code-block:: python

        Hello Mixly!
        Hello mixly! i love mixly!
        hELLO mIXLY!
        hello mixly!

9.字符串填充对齐
-----------------

.. image:: images/05Text/string_alignment.png
9.1 描述
++++
    此模块可以在指定宽度中对字符串进行左对齐、居中和右对齐，并使用指定的符号填充空白部分。

.. attention::
    指定的符号可以为空格，但不可以为空，否则会报错。

9.2 示例
++++

    .. image:: images/05Text/string_alignment_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print("Hello,mixly".ljust(50,"*"))
            print("I love mixly!".center(50,"*"))
            print("Happy mixly!".rjust(50,"*"))
            print("Hello,mixly".ljust(50," "))
            print("I love mixly!".center(50," "))
            print("Happy mixly!".rjust(50," "))

    输出结果：

    .. code-block:: python

        Hello,mixly***************************************
        ******************I love mixly!*******************
        **************************************Happy mixly!
        Hello,mixly
                          I love mixly!
                                              Happy mixly!

10.字符串搜索
-----------------

.. image:: images/05Text/string_get_index.png
10.1 描述
++++
    此模块可以检查字符串中是否包含指定字符或者字符串，如果包含则返回字符或字符串首字符的索引值，否则返回-1。

.. attention::
    * 如果有多个检索结果，则返回第一个检索结果的索引。
    * 字符串各个字符的索引从0开始，也就是说第一个字符的索引为0，第九个字符的索引为8，以此类推。
    * 可以在字符串中检索字符和字符串。
    * 检索时是严格匹配大小写的。

10.2 示例
++++

    .. image:: images/05Text/string_get_index_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print("Hello,mixly".find("l"))
            print("Hello,mixly".find("ll"))
            print("Hello,mixly".find("mix"))
            print("Hello,mixly".find("h"))

    输出结果：

    .. code-block:: python

        2
        2
        6
        -1

11.序列连接成字符串
-----------------

.. image:: images/05Text/line_link.png
11.1 描述
++++
    此模块可以使用指定的连接字符或字符串将序列（列表、元组、集合等）的各个元素连接起来形成并返回一个字符串。

.. attention::
    * 需要连接的序列（列表、元组、集合等）的每一个元素必须是字符串的数据类型。
    * 集合（{}）是无序序列，所以每次对集合进行连接返回的字符串是随机的。

11.2 示例
++++

    .. image:: images/05Text/line_link_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(" ".join(["Beijing", "Normal", "University"]))
            print("-".join(("Mixly", "Mixpy", "MixGo")))
            print("--".join({"Mixly", "Mixpy", "MixGo"}))

    输出结果：

    .. code-block:: python

        Beijing Normal University
        Mixly-Mixpy-MixGo
        MixGo--Mixly--Mixpy     # 此项由于是连接集合，所以每一次输出的结果是随机的

12.字符串内容替换
-----------------

.. image:: images/05Text/string_replace.png
12.1 描述
++++
    此模块可以将字符串中的某一字符串替换为另一字符串，返回替换后的字符串。

.. attention::
    * 此模块的替换操作并不会改变原字符串。
    * 此模块在替换时会替换所有符合条件的字符串。

12.2 示例
++++

    .. image:: images/05Text/string_replace_example.png

    源代码：

    .. code-block:: python
            :linenos:

            str = "Hello,mixly"
            print(str.replace("mixly","mixpy"))
            print(str)
            print("北京师范大学米思齐团队".replace("米思齐","Mixly"))
            print("Hello,mixly".replace("l","A"))

    输出结果：

    .. code-block:: python

        Hello,mixpy
        Hello,mixly
        北京师范大学Mixly团队
        HeAAo,mixAy

13.字符串分割
-----------------

.. image:: images/05Text/string_split.png
13.1 描述
++++
    此模块通过指定分隔符对字符串进行分割（切片），返回一个列表。

.. attention::
    * 分隔符将会被删除，不会存在于结果列表中。
    * 分隔符是严格区分大小写的。

13.2 示例
++++

    .. image:: images/05Text/string_split_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print("I love mixly!".split(" "))
            print("Hello mixly!".split("l"))
            print("Hello mixly!".split("L"))
            print("北京师范大学米思齐团队".split("米思"))

    输出结果：

    .. code-block:: python

        ['I', 'love', 'mixly!']
        ['He', '', 'o mix', 'y!']
        ['Hello mixly!']
        ['北京师范大学', '齐团队']

14.字符串消除空格
-----------------

.. image:: images/05Text/string_delete_space.png
14.1 描述
++++
    此模块可以消除字符串两侧或者单独删除左、右侧的空格，返回处理后的字符串。

14.2 示例
++++

    .. image:: images/05Text/string_delete_space_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(" I love mixly ".strip())
            print(" I love mixly ".lstrip())
            print(" I love mixly ".rstrip())

    输出结果：

    .. code-block:: python

        I love mixly
        I love mixly
         I love mixly

15.字符串格式化
-----------------

.. image:: images/05Text/string_format.png
15.1 描述
++++
    此模块可以在字符串中格式化传递参数。需要传递参数的地方使用"{}"标记，你可以点击模块上的蓝色齿轮添加多个参数。

.. attention::
    * 传递的参数可以是各种数据类型。
    * 后面参数排列的顺序对应"{}"标记的顺序。

15.2 示例
++++

    .. image:: images/05Text/string_format_example.png

    源代码：

    .. code-block:: python
            :linenos:

            import random
            place = ["北京", "重庆", "广东", "浙江", "福建"]
            MixGroup = ["Mixly", "Mixpy", "MixGo"]
            print("Random No. is {}".format(random.randint(1, 100)))
            print("我在{}，我爱{}！".format(random.choice(place), random.choice(MixGroup)))

    输出结果：

    .. code-block:: python

        Random No. is 62
        我在重庆，我爱Mixly！

16.字符串表达式执行
-----------------

.. image:: images/05Text/string_execute.png
16.1 描述
++++
    此模块可以执行一个字符串表达式，并返回表达式的值。

.. attention::
    表达式指的是加减乘除、数学函数运算、逻辑运算等

16.2 示例
++++

    .. image:: images/05Text/string_execute_example.png

    源代码：

    .. code-block:: python
            :linenos:

            a = True
            b = False
            c = 3
            d = 5
            print(eval("1+1"))
            print(eval("pow(2,2)"))     # 2的2次方
            print(eval("int('1') * 3"))
            print(eval("a and b"))
            print(eval("c * d"))
            print(eval("d > c"))

    输出结果：

    .. code-block:: python

        2
        4
        3
        False
        15
        True

.. _string_index:

附录：字符串索引方向
-----------------
.. attention::
    * 有正反两种索引方式。
    * 正向索引中，字符串各个字符的索引从0开始，也就是说第一个字符的索引为0，第九个字符的索引为8，以此类推。
    * 反向索引中，字符串各个字符的索引从字符串长度的相反数开始，也就是说，对于一个长度为5的字符串，第一个（倒数第五个）字符的索引为-5，倒数第一个（最后一个）字符的索引为-1，倒数第二个字符的索引为-2，以此类推。
    * 阅读完毕后，在浏览器中后退可回到之前的浏览位置。