通信
====================

通信是为了方便人与计算机交互而采用的一种特殊通信方式。具体包括：串口通信(新增串口选择和波特率设置)、红外通信、I2C通信、SPI通信（新增）。

.. image:: images/18Communicate/communicate.png

1.板载IIC
------------------

.. image:: images/18Communicate/board_iic.png

1.1描述
++++++++
返回IIC对象，具体功能待验证

2.获取红外接收值
------------------

.. image:: images/18Communicate/get_ir_data.png

2.1描述
++++++++
以十六进制或原始输出获取红外接收值。

3.红外发射
------------------

.. image:: images/18Communicate/send_ir_data.png

3.1描述
++++++++
以十六进制或原始输出格式发送数据。

4.无线插座初始化
------------------

.. image:: images/18Communicate/wire_init.png

5.无线插座状态改变
--------------------
.. image:: images/18Communicate/ws_on.png

6.创建某长度缓冲器
--------------------
.. image:: images/18Communicate/buf.png

7.IIC初始化
--------------------

.. image:: images/18Communicate/iic_init.png

7.1描述
++++++++
使用IIC通信协议时可在通信线路上同时挂载多个设备，因此这里介绍的I2C指令即用于多个设备与主控器间的数据传输，关于IIC更多信息，请自行网上搜索下。
I2C初始化及使用I2C条指令需同时使用，首先，I2C初始化在实际使用中须关注SCL和SDA的两个参数的设置需要根据所连接的扩展口进行设置，例如，这里连接U1口时，SCL设置为1，SDA设置为2,注意顺序，先小后大；至于初始化后的实例名及频率一般默认即可。
其次，使用I2C时将上面初始化的实例名，并根据实际使用自定义改变“xsensor”实例化对象名，频率默认即可。

8.IIC占用总线
--------------------

.. image:: images/18Communicate/iic_lock.png

8.1描述
++++++++
在起始信号开始后，总线就处于被占用状态，在终止信号产生后，总线就处于空闲状态。

9.搜索IIC总线上的设备
-----------------------

.. image:: images/18Communicate/iic_scan.png

10.向某个地址传输数据
-----------------------
.. image:: images/18Communicate/iic_write_to.png

11.从某个地址读取数据至buf
---------------------------

.. image:: images/18Communicate/iic_read.png

12.向某个地址写入并保存读到的数据
---------------------------------
.. image:: images/18Communicate/iic_write_then.png

13.SPI释放总线
---------------------------------
.. image:: images/18Communicate/spi_unlock.png

14.IIC释放总线
---------------------------------
.. image:: images/18Communicate/iic_unlock.png

15.SPI初始化
---------------------------------
.. image:: images/18Communicate/spi_init.png

15.1描述
+++++++++
SPI是串行外设接口（Serial Peripheral Interface）的缩写。SPI，是一种高速的，全双工，同步的通信总线，并且在芯片的管脚上只占用四根线，节约了芯片的管脚，同时为PCB的布局上节省空间，提供方便，拥有简单易用的特性。用户可以使用Mixly向SPI传输数据。

16.SPI波特率相关设置
---------------------------------
.. image:: images/18Communicate/spi_config.png

17.SPI占用总线
---------------------------------
.. image:: images/18Communicate/spi_lock.png

18.SPI写入数据
---------------------------------
.. image:: images/18Communicate/spi_write.png

19.SPI读取数据至buf
---------------------------------
.. image:: images/18Communicate/spi_read.png

20.SPI写入数据同时读取数据至buf
---------------------------------
.. image:: images/18Communicate/spi_write_into.png

21.SPI释放总线
---------------------------------
.. image:: images/18Communicate/spi_unlock2.png

22.OneWire初始化
---------------------------------
.. image:: images/18Communicate/one_init.png

23.OneWire读取比特数据
---------------------------------
.. image:: images/18Communicate/one_read.png

24.OneWire写入比特数据
---------------------------------
.. image:: images/18Communicate/one_write.png

25.OneWire重置
---------------------------------
.. image:: images/18Communicate/one_reset.png
