控制
=================

控制类别中包括了作为主程序运行、当型重复执行、延时函数、如果判断、序列循环、直到型循环执行、占位语句、跳出循环、尝试执行、系统运行时间、获取格式化时间、NTP获取当前时间等
对程序结构进行相应的控制语句。

.. image:: images/02Control/control.png

1.作为主程序运行
-----------------------

.. image:: images/02Control/setup.png

.. code-block:: python
        :linenos:

        if __name__ == '__main__':
            pass

    


1.1 描述
++++

	if __name__=="__main__":就相当于是 Python 模拟的程序入口。由于模块之间相互引用，不同模块可能都有这样的定义，而入口程序只能有一个，选中哪个入口程序取决于 __name__ 的值。

1.2 范例1
++++

只要你创建了一个模块（一个.py文件），这个模块就有一个内置属性name生成，该模块的 name 的值取决于如何应用这个模块。也就是，如果你直接运行该模块，那么__name__ == "__main__"；如果你 import 一个模块，那么模块name 的值通常为模块文件名。

如，创建一个test1.py:

    .. image:: images/02Control/test1.png

    源代码：

    .. code-block:: python
            :linenos:

            def func():
                print('hello, world!')

            if __name__ == "__main__":
                func()

	


模块中，首先定义了函数func()，用来打印出hello, world!，然后判断__name__ 是否等于 __main__，如果等于，有打印，反之则反，现在运行该模块，结果为：hello, world!
说明__name__ 等于 __main__。

这时，再创建一个test2.py:

.. code-block:: python
        :linenos:

        import test1
        print('good world!')

模块中，首先import test1，然后打印good world!做测试用，运行该模块，结果为:good world!
运行结果仅有good world!，说明__name__ 不等于 __main__。

.. Attention::
	通过上面test1.py和test2.py两个模块，可以得出一个结论: 如果模块是被直接运行的，则if __name__ == "__main__":后的代码块被运行，如果模块被import，则代码块不被运行。

2.当型重复执行
---------------

.. image:: images/02Control/do_while1.png
    
源代码:

.. code-block:: python
        :linenos:

        while True:
            pass

2.1 描述
++++++++

while循环会无限的循环，直到括号内的判断语句变为假。
	必须要有能改变判断语句的东西，要不然while循环将永远不会结束。你可以使用一个传感器的值，或者一个变量来控制什么时候停止该循环。

2.2 参数
++++++++

* 满足条件:为真或为假的一个条件。

2.3 范例
++++++++

    .. image:: images/02Control/do_while2.png

当板载温度低于30度时，亮灯，否则灭灯。

.. code-block:: python
        :linenos:
            
        import sensor
        from led import led_L1

        while sensor.get_temperature() < 0:
            led_L1.setonoff(1)
            led_L1.setonoff(0)

3.延时
---------------

.. image:: images/02Control/delay.png

源代码:

.. code-block:: python
        :linenos:

        import time

        time.sleep(1)

3.1 描述
++++++++++++++

使程序暂在该处暂停等待设定的时间（单位秒）。（一秒等于1000毫秒）。

3.2 参数
+++++++++++++++
* 秒:暂停的秒数。

3.3 范例
+++++++++

板载L1灯亮1秒，灭1秒，往复循环。

.. image:: images/02Control/delay_example.png

.. code-block:: python
        :linenos:

        from led import led_L1
        import time


        while True:
            led_L1.setonoff(1)
            time.sleep(1)
            led_L1.setonoff(1)
            time.sleep(1)


.. Attention::
	虽然创建一个使用time.sleep()的闪烁LED很简单，并且许多例子将很短的延时用于消除开关抖动。
	但延时函数确实拥有很多显著的缺点。在time.sleep()函数使用的过程中，读取传感器值、计算、引脚操作均无法执行，因此，它所带来的后果就是使其他大多数活动暂停。大多数熟练的程序员通常避免超过10毫秒的延时,除非程序非常简单。
	

4.如果(if)
---------------

.. image:: images/02Control/if.png

.. code-block:: python
        :linenos:

        if False:
            pass

4.1 描述
++++++++++++++

	if 语句与比较运算符一起用于检测某个条件是否达成，如某个传感器的值是否等于某个值。

4.2 参数
+++++++++++++++
* 条件：比较表达式

4.3 用法
+++++++++++++++
增加条件：如果需要增加条件，可以点开齿轮，然后将左侧的“否则如果”或者“否则”模块拖到右侧的“如果”之中。

.. image:: images/02Control/if-2.png

4.4 范例1
+++++++++

当连接在B1引脚的按键按下时，点亮板载L1灯。

.. image:: images/02Control/if_example1.png

.. code-block:: python
        :linenos:

        from button import button_B1
        from led import led_L1


        while True:
            if button_B1.was_pressed():
                led_L1.setonoff(1)

如果判断的结果，只要该表达式的结果不为0，则为真。



4.5 范例2
+++++++++

当连接在B1引脚的按键按下时，点亮板载L1灯;当按键松开时，灯灭。

.. image:: images/02Control/if_example2.png

.. code-block:: python
        :linenos:

        from button import button_B1
        from led import led_L1


        while True:
            if button_B1.was_pressed():
                led_L1.setonoff(1)
            else:
                led_L1.setonoff(0)


5.序列循环(for)
---------------

.. image:: images/02Control/for.png

.. code-block:: c
        :linenos:

        for i in range(0, 5, 1):
            pass

5.1 描述
++++++++++++++

	代表从0到5，间隔1,不包括5，即左闭右开[0, 5);间隔1可以是负数，有时这也叫做'步长'。

5.2 参数
+++++++++++++++
* 变量名：用于记录for循环次数的变量名。
* 起始值：循环的计数起始值，一般从0开头，也可以从其他数值开始。
* 终点值：循环的计数终点值。
* 步长：每次循环的步长，一般为1，也可以是其他整数。
* 
5.4 范例
+++++++++

串口输出i

.. image:: images/02Control/for_example.png

.. code-block:: c
        :linenos:

        for i in range(0, 5, 1):
            print(i)


串口输出结果：0，1，2，3，4

6.直到型循环
---------------

.. image:: images/02Control/while_do.png

.. code-block:: c
        :linenos:

        while True:
            if (False):
                break

6.1 描述
++++++++++++++

	该循环语句与当型循环唯一区别在于多了条件判断，也就是当条件满足为真时，则停止该循环运行。


6.2 范例
+++++++++


定义一个变量状态，实现一个循环5次点亮熄灭板载LED灯的测试程序，其中每执行一次循环状态值增加1，直至状态变量的值等于5则停止运行该循环。

.. image:: images/02Control/for_example.png

.. code-block:: python
        :linenos:

        from led import led_L1
        import time

        状态 = False
        while True:
            led_L1.setonoff(1)
            time.sleep(1)
            led_L1.setonoff(0)
            time.sleep(1)
            if (状态 == True):
                break

.. Attention::
	直到型循环，中内嵌了一个if条件判断，仅当表达式返回为真时，则跳出该循环的运行。

7.占位语句
---------------

.. image:: images/02Control/pass.png

.. code-block:: c
        :linenos:

        pass

7.1 描述
++++++++++++++

	pass 是一个空操作，当它被执行时，什么都不发生。它适合当语法上需要一条语句但并不需要执行任何代码时用来临时占位 在编写代码时只写框架思路，具体实现还未编写就可以用pass进行占位，使程序不报错，不会进行任何操作。

7.2 范例
+++++++++
pass在循环程序中占位。

.. image:: images/02Control/pass_example.png


.. code-block:: python
        :linenos:

        while True:
            pass



8.跳出循环
-------------

.. image:: images/02Control/break.png

.. code-block:: python
        :linenos:

        break

8.1 描述
++++++++++++++

	跳出循环用于终止一段重复的程序，一般使用时作为条件语句的执行部分，当循环中的变量满足某个条件时，执行跳出循环语句。

	跳出循环在处理循环中的特殊情况时十分有用。

8.2 参数
+++++++++++++++
* 操作：可以选择跳出循环和跳到下一个循环两种操作，结果不同。

8.3 范例
+++++++++

	在序列for循环中，当i为500时，串口输出“500”，并停止增加

.. image:: images/02Control/break_example.png

.. code-block:: python
        :linenos:

        for i in range(0, 1000, 1):
            if i == 500:
                print('500')
                break

.. Attention::
	注意跳到下一个循环的使用，可以方便的将循环中不需要的步骤跳过。

9.尝试执行
----------------------

.. image:: images/02Control/try.png

.. code-block:: python
        :linenos:

        try:
            pass
        except:
            pass

9.1 描述
++++++++++++++

	捕捉异常可以使用try/except语句。try/except语句用来检测try语句块中的错误，从而让except语句捕获异常信息并处理。
	如果你不想在异常发生时结束你的程序，只需在try里捕获它。

9.2 范例
++++++++++++++

	在物联网WIFI连接过程中，容易出现连接异常而导致程序无法正常向下运行，为让程序更加稳定，现在通过try-except进行实现，为清晰
	看到该捕获异常的具体效果，现要连接到一无线网中，wifi信息为：用户名1234，密码12345678，通过程序可以看到在try中的密码为错
	误的，except为正确的，此时当程序运行到第一条wifi连接指令时，便出错，导致下面的一条串口指令无法打印，若未使用try-except情
	况下，程序将直接崩溃，而现在将继续运行except中程序。好好体会下有无try-except的区别。

.. image:: images/02Control/try_example.png

.. code-block:: python
        :linenos:

        import wifi


        try:
            wifi.radio.connect('1234','123456789')
            print('WOK')
        except:
            wifi.radio.connect('1234','12345678')
        while True:
            pass

10.系统运行时间
---------------

.. image:: images/02Control/system_time.png

.. code-block:: python
        :linenos:

        import time


        time.monotonic()

10.1 描述
++++++++++++++

	返回自硬件启动或重启以来的时间值。

10.2 返回
+++++++++++++++
自硬件启动或重启以来的时间，毫秒数。

10.3 范例
+++++++++

自动换行打印系统运行时间

.. image:: images/02Control/time_example.png

.. code-block:: c
        :linenos:

        import time


        while True:
            time.sleep(1)
            print(time.monotonic())


11.获取格式化时间
---------------

.. image:: images/02Control/for_time.png

.. code-block:: python
        :linenos:

        import time


        time.localtime()

11.1 描述
++++++++++++++

	获取本地当前时间信息。

11.2 范例
+++++++++

	获取当前年份信息

.. image:: images/02Control/for_year.png

.. code-block:: python
        :linenos:

        import time


        print(time.localtime()[0])

