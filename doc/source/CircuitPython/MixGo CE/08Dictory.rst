字典
======================
.. note::
    字典是另一种可变容器模型，且可存储任意类型对象。

    字典的每个键值对(key=>value)用冒号(:)分割，每个对之间用逗号(,)分割，整个字典包括在花括号({})中，格式如下所示：

    .. code-block:: python
            :linenos:

            d = {key1 : value1, key2 : value2 }

    键必须是唯一的，但值则不必。

    值可以取任何数据类型，但键必须是不可变的，如字符串，数字或元组。

    Mixly中有很多用于元组操作的模块，如下：

.. image:: images/08Dictory/dictories.png

1.字典初始化
-----------------

.. image:: images/08Dictory/dict_initial.png
1.1 描述
++++
    在初始化字典时，你需要输入字典和各个键的名称，然后将代表各种数据的模块对应地连接到初始化模块上与键对应的地方，你也可以点击蓝色齿轮增加键值对数量。

    在初始化字典后，你可以在"变量"模块分类中根据你输入的字典名称找到代表此字典的模块。

.. attention::
    键必须是唯一且不可变的。

1.2 示例
++++

    .. image:: images/08Dictory/dict_initial_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict)

    输出结果：

    .. code-block:: python

        {'Name': 'Fred', 'Age': 18, 'Country': 'China', 'ID': 1}

2.字典获取所有键
-----------------

.. image:: images/08Dictory/dict_get_keys.png
2.1 描述
++++
    此模块可以返回一个字典所有的键的名称。

.. attention::
    此模块返回的是一个名叫 'dict_keys' 类的对象，你可以将其转换为列表对象。

2.2 示例
++++

    .. image:: images/08Dictory/dict_get_keys_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict.keys())
            print(type(mydict.keys()))
            print(list(mydict.keys()))


    输出结果：

    .. code-block:: python

        dict_keys(['Name', 'Age', 'Country', 'ID'])     # 'dict_keys' 类对象
        <class 'dict_keys'>     # 对象的类别
        ['Name', 'Age', 'Country', 'ID']        # 转换为列表后

3.字典获取键值
-----------------

.. image:: images/08Dictory/dict_get_value.png
3.1 描述
++++
    此模块可以返回字典中指定键的键值。

.. attention::
    * 若指定的键不存在于字典中，则会报错。
    * 字典键的名称是严格区分大小写的。

3.2 示例
++++

    .. image:: images/08Dictory/dict_get_value_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict["Name"])
            print(mydict["Age"])

    输出结果：

    .. code-block:: python

        Fred
        18

4.字典添加或修改键值
-----------------

.. image:: images/08Dictory/dict_add_upgrade.png
4.1 描述
++++
    此模块可以对字典中给定的键对应的值进行修改，若给定的键不存在于字典中，则此键值对将会被添加到字典中。

.. attention::
    此模块没有返回值。

4.2 示例
++++

    .. image:: images/08Dictory/dict_add_upgrade_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict["Name"])
            mydict["Name"] = "Sam"
            print(mydict["Name"])
            mydict["Gender"] = "Male"
            print(mydict["Gender"])
            print(mydict)

    输出结果：

    .. code-block:: python

        Fred
        Sam
        Male
        {'Name': 'Sam', 'Age': 18, 'Country': 'China', 'ID': 1, 'Gender': 'Male'}

5.字典删除键值
-----------------

.. image:: images/08Dictory/dict_delete_key.png
5.1 描述
++++
    此模块可以在字典中根据所给键删除键值对。

.. attention::
    * 若所给键值不存在于字典中，则会报错。
    * 此模块没有返回值。

5.2 示例
++++

    .. image:: images/08Dictory/dict_delete_key_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict)
            del mydict["ID"]
            print(mydict)

    输出结果：

    .. code-block:: python

        {'Name': 'Fred', 'Age': 18, 'Country': 'China', 'ID': 1}
        {'Name': 'Fred', 'Age': 18, 'Country': 'China'}

6.字典删除并返回键值
-----------------

.. image:: images/08Dictory/dict_pop_key.png
6.1 描述
++++
    此模块可以在字典中根据所给键返回键值，并删除键值对。

.. attention::
    * 若所给键值不存在于字典中，则会报错。
    * 此模块没有返回值。

6.2 示例
++++

    .. image:: images/08Dictory/dict_pop_key_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict.pop("ID"))
            print(mydict)

    输出结果：

    .. code-block:: python

        1
        {'Name': 'Fred', 'Age': 18, 'Country': 'China'}

7.字典设置键默认值
-----------------

.. image:: images/08Dictory/dict_set_default.png
7.1 描述
++++
    此模块可以为字典中给定的键设定默认值。
    若给定的键存在于字典中，则对应的值不会改变；
    若给定的键不存在于字典中，则键值对将会被添加到字典中，且键值为所给的默认值。

.. attention::
    此模块没有返回值。

7.2 示例
++++

    .. image:: images/08Dictory/dict_set_default_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict)
            mydict.setdefault("Class",1)
            print(mydict)
            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict)
            mydict.setdefault("ID",0)
            print(mydict)

    输出结果：

    .. code-block:: python

        {'Name': 'Fred', 'Age': 18, 'Country': 'China', 'ID': 1}
        {'Name': 'Fred', 'Age': 18, 'Country': 'China', 'ID': 1, 'Class': 1}
        {'Name': 'Fred', 'Age': 18, 'Country': 'China', 'ID': 1}
        {'Name': 'Fred', 'Age': 18, 'Country': 'China', 'ID': 1}

8.字典清空条目
-----------------

.. image:: images/08Dictory/dict_clear.png
8.1 描述
++++
    此模块可以清空给定字典的条目。

.. attention::
    * 字典被清空后，只是变成了不包含条目的空字典，此字典对象仍然存在，可以使用字典名调用。
    * 此模块没有返回值。

8.2 示例
++++

    .. image:: images/08Dictory/dict_clear_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict)
            mydict.clear()
            print(mydict)

    输出结果：

    .. code-block:: python

        {'Name': 'Fred', 'Age': 18, 'Country': 'China', 'ID': 1}
        {}

9.字典转换为列表
-----------------

.. image:: images/08Dictory/dict_to_list.png
9.1 描述
++++
    此模块可以将字典转换为键值对元组作为元素的 'dict_items' 类对象，你可以将其转换为列表对象。

.. attention::
    此模块返回的是一个名叫 'dict_items' 类的对象，你可以将其转换为列表对象。

9.2 示例
++++

    .. image:: images/08Dictory/dict_to_list_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict)
            print(mydict.items())
            print(type(mydict.items()))
            print(list(mydict.items()))

    输出结果：

    .. code-block:: python

        {'Name': 'Fred', 'Age': 18, 'Country': 'China', 'ID': 1}
        dict_items([('Name', 'Fred'), ('Age', 18), ('Country', 'China'), ('ID', 1)])        # 'dict_items' 类对象
        <class 'dict_items'>        # 'dict_items' 类对象名
        [('Name', 'Fred'), ('Age', 18), ('Country', 'China'), ('ID', 1)]

10.字典获取所有键值
-----------------

.. image:: images/08Dictory/dict_get_values.png
10.1 描述
++++
    此模块可以返回一个字典所有的键值。

.. attention::
    此模块返回的是一个名叫 'dict_values' 类的对象，你可以将其转换为列表对象。

10.2 示例
++++

    .. image:: images/08Dictory/dict_get_values_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict)
            print(mydict.values())
            print(type(mydict.values()))
            print(list(mydict.values()))


    输出结果：

    .. code-block:: python

        {'Name': 'Fred', 'Age': 18, 'Country': 'China', 'ID': 1}
        dict_values(['Fred', 18, 'China', 1])       # 'dict_values' 类对象
        <class 'dict_values'>       # 对象的类别
        ['Fred', 18, 'China', 1]        # 转换为列表后

11.字典获取长度
-----------------

.. image:: images/08Dictory/dict_get_length.png
11.1 描述
++++
    此模块可以返回一个字典的长度（条目数量）。

.. attention::
    此模块返回的长度是int类型的值。

11.2 示例
++++

    .. image:: images/08Dictory/dict_get_length_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict)
            print(len(mydict))


    输出结果：

    .. code-block:: python

        {'Name': 'Fred', 'Age': 18, 'Country': 'China', 'ID': 1}
        4

12.删除字典
-----------------

.. image:: images/08Dictory/dict_delete.png
12.1 描述
++++
    此模块可以删除指定的字典。

.. attention::
    * 此模块没有返回值。
    * 删除字典后，无法再使用已被删除的字典名称调用字典，否则会报错。

12.2 示例
++++

    .. image:: images/08Dictory/dict_delete_example.png

    源代码：

    .. code-block:: python
            :linenos:

            mydict= {"Name":"Fred", "Age":18, "Country":"China", "ID":1}
            print(mydict)
            del mydict

    输出结果：

    .. code-block:: python

        {'Name': 'Fred', 'Age': 18, 'Country': 'China', 'ID': 1}

