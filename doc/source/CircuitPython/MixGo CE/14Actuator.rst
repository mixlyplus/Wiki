板载执行
===================
执行模块：内嵌LED灯状态设置获取及亮度控制指令、播放声音、结束声音、播放音符列表、设音频节奏、获取音频节奏、恢复音乐初始设置、RGB等颜色模式设置、RGB灯整体控制、RGB灯独立控制、RGB跑马灯、RGB灯彩虹效果控制、RGB灯生效等。总体来说该部分指令可分为内嵌LED灯控制、蜂鸣器声音控制及RGB灯控制等三个部分指令。

.. image:: images/14Actuator/actuator.png

1.内嵌LED灯控制
-----------------------

.. image:: images/14Actuator/leds.png

内嵌LED灯控制指令主要包括：状态设置、状态获取、亮度设置三个功能指令。

1.1内嵌LED灯状态控制
-----------------------

.. image:: images/14Actuator/led.png

.. code-block:: python
        :linenos:

        from led import led_L1


        led_L1.setonoff(1)

1.2 描述
++++++++++++

内嵌LED灯状态控制，用于初始化并设置内嵌LED灯状态控制，其中包含两个参数，一个是L1与L2可选，对应控制板载L1和L2灯状态，另外就是LED灯具体状态控制，包括：亮、灭和反转三种状态，前两者字面意思不再解释，反转这里是指基于上一次状态进行非运算点亮或熄灭LED灯的状态，也就是上一次状态为亮，则运行反转则为灭，否则反之。

1.3 范例
++++++++++++

设置点亮或熄灭内嵌LED灯L1,并通过串口打印所获取L1灯的状态。

如:

    .. image:: images/14Actuator/led_example.png

    源代码：

    .. code-block:: python
            :linenos:

            from led import led_L1


            led_L1.setonoff(-1)
            print(led_L1.getonoff())
    
* led_L1.getonoff()函数返回数字量，0或1

1.4 设置内嵌LED灯亮度
-----------------------

.. image:: images/14Actuator/led_light.png

.. code-block:: python
        :linenos:

        from led import led_L1


        led_L1.setbrightness(65535)

2.1 描述
++++++++++++

通过占空比控制LED灯的亮度变化，16位精度，即取值范围为0-65535。

2.2范例
++++++++++++

借助for循环实现呼吸灯效果。

.. image:: images/14Actuator/led_pwm.png

    .. code-block:: python
        :linenos:

        from led import led_L1


        while True:
            for i in range(0, 65535, 20):
                led_L1.setbrightness(i)
            for i in range(65535, 0, -20):
                led_L1.setbrightness(i)
            
* 注意步长一正一负。

3.播放声音
-----------------------

.. image:: images/14Actuator/buzzer.png

.. code-block:: python
        :linenos:

        from music import buzzer


        buzzer.play(440)

3.1 描述
++++++++++++

MixGo CE板载蜂鸣器，buzzer.play()进行驱动，其中频率参数软件中给的设定范围为262-1796，可根据使用场景自行设置测试具体数值。

* 注意该函数执行后，蜂鸣器会一直发出最后执行频率的声音，若要暂定该声音，需执行结束声音指令。
  
.. image:: images/14Actuator/buzzer_3.png

3.2范例
++++++++++++

通过蜂鸣器循环顺序发出do re mi fa so la xi。

.. image:: images/14Actuator/buzzer_example.png

    .. code-block:: python
        :linenos:

        from music import buzzer
        import time


        while True:
            buzzer.play(262)
            time.sleep(1)
            buzzer.play(294)
            time.sleep(1)
            buzzer.play(330)
            time.sleep(1)
            buzzer.play(349)
            time.sleep(1)
            buzzer.play(392)
            time.sleep(1)
            buzzer.play(440)
            time.sleep(1)
            buzzer.play(494)
            time.sleep(1)

3.3播放声音持续时间
-----------------------

.. image:: images/14Actuator/buzzer_ex2.png

.. code-block:: python
        :linenos:

        from music import buzzer


        buzzer.play(440, 1000) 

3.4 播放音符列表
++++++++++++++++++

.. image:: images/14Actuator/buzzer_4.png

MixGo CE固件内置了一些音频效果，Mixly软件里即可快速调用。

3.5 待测完善
++++++++++++

待测。

.. image:: images/14Actuator/buzzer_5.png

4.RGB灯控制指令
-----------------------

.. image:: images/14Actuator/rgb.png

4.1 设置颜色模式
+++++++++++++++++

为兼容版本间差异，这里有两种模式，RGB和GRB,当发现RGB灯的红色和绿色两种颜色无法按程序正常显示时，使用该句进行初始化即可。

.. image:: images/14Actuator/rgb_1.png

.. code-block:: python
        :linenos:

        from pixels import rgb


        rgb.change_mod("RGB")

4.2 范例
++++++++++++

循环顺序变化RGB灯颜色，依次为红、绿、蓝。

.. image:: images/14Actuator/rgb_2.png

    .. code-block:: python
        :linenos:

        from pixels import rgb
        import time


        rgb.change_mod("GRB")
        while True:
            rgb.show_all(125, 0, 0)
            rgb.write()
            time.sleep(1)
            rgb.show_all(0, 125, 0)
            rgb.write()
            time.sleep(1)
            rgb.show_all(0, 0, 125)
            rgb.write()
            time.sleep(1)

* 注意RGB生效指令的使用，否则就无法看到效果。
  
4.3 RGB独立控制指令
++++++++++++++++++++

该指令通过灯号独立控制4颗RGB灯的点亮效果，注意仍需RGB生效指令，否则效果无法显示。

.. image:: images/14Actuator/rgb_3.png

4.4 范例
++++++++++++++++++++

通过灯号独立控制4颗RGB灯，1-4逐一点亮。

.. image:: images/14Actuator/rgb_33.png

    .. code-block:: python
        :linenos:

        from pixels import rgb
        import time


        rgb.change_mod("GRB")
        rgb.show_one(1, 255, 0, 0)
        rgb.write()
        time.sleep(1)
        rgb.show_one(2, 255, 0, 0)
        rgb.write()
        time.sleep(1)
        rgb.show_one(3, 255, 0, 0)
        rgb.write()
        time.sleep(1)
        rgb.show_one(4, 255, 0, 0)
        rgb.write()
        time.sleep(1)


4.5 RGB跑马灯
-----------------------

.. image:: images/14Actuator/rgb_4.png

.. code-block:: python
        :linenos:

        from pixels import rgb


        rgb.color_chase(0, 0, 0, 1000)

4.6 描述
++++++++++++

该指令根据设定的RGB灯颜色，从1-4号逐一变换效果，间隔时间参数为跑马灯延时时长，注意此函数无需RGB生效指令，即可正常显示。

4.7 范例
++++++++++++

红蓝跑马灯效果。

.. image:: images/14Actuator/rgb_5.png

    .. code-block:: python
        :linenos:

        from pixels import rgb


        rgb.change_mod("GRB")
        while True:
            rgb.color_chase(125, 0, 0, 1000)
            rgb.color_chase(0, 0, 125, 1000)

4.8 RGB 彩虹效果
-----------------------

.. image:: images/14Actuator/rgb_66.png

.. code-block:: python
        :linenos:

        from pixels import rgb


        rgb.change_mod("GRB")
        rgb.rainbow_cycle(1000)

* 无需RGB生效指令，4颗RGB灯出现类似彩虹的效果显示，持续时间内，是动态变化的。
