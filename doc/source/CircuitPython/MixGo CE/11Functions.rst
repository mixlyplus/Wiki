函数
======================
.. note::
    Mixly中，函数是组织好的、可重复使用的、用来实现单一或相关联功能的一组模块。

    函数能提高应用的模块性和模块的重复利用率。

    你已经知道Mixly提供了许多内建模块，比如"打印（自动换行）"。

    你可以自己创建函数，这被叫做自定义函数。

    Mixly中有如下用于函数操作的模块：

.. image:: images/11Functions/functions.png

1.函数定义（无返回值）
-----------------

.. image:: images/11Functions/function_define.png
1.1 描述
++++
    Mixly中，你需要在函数定义模块中输入函数的名称，然后将实现函数功能的各种其他模块连接到函数定义模块中。

.. attention::
    * 函数定义模块默认没有参数，你可以点击蓝色齿轮为函数增加参数；
    * 请尽量不要为函数命名为无意义的名称，函数的名称最好能够清楚地体现函数的功能；
    * 当你创建有参数的函数后，你可以在变量分类下找到调用该参数的模块；
    * 当你创建函数后，你可以在函数分类下找到调用函数的模块。

1.2 示例
++++

    .. image:: images/11Functions/function_define_example.png

    源代码：

    .. code-block:: python
            :linenos:

            def welcome(name):
                print("你好，" + str(name) + "！\n欢迎使用Mixly！")
            welcome("Fred")

    输出结果：

    .. code-block:: python

        你好，Fred！
        欢迎使用Mixly！

2.函数定义（有返回值）
-----------------

.. image:: images/11Functions/function_return_define.png
2.1 描述
++++
    在定义有返回值的函数时，你需要在返回值旁边连接变量模块。

    有返回值的函数调用模块可以作为变量连接到其他模块上。

.. attention::
    * 函数定义模块默认没有参数，你可以点击蓝色齿轮为函数增加参数；
    * 请尽量不要为函数命名为无意义的名称，函数的名称最好能够清楚地体现函数的功能；
    * 当你创建有参数的函数后，你可以在变量分类下找到调用该参数的模块；
    * 当你创建函数后，你可以在函数分类下找到调用函数的模块。

2.2 示例
++++

    定义了两个有返回值的函数，square用于计算参数的平方值，calcu_1-2k在给定一元二次方程的参数a, b, c 后可以直接计算出方程的解，并返回一个解组成的列表。

    .. image:: images/11Functions/function_return_define_example.png

    源代码：

    .. code-block:: python
            :linenos:

            import math
            def square(x):
                z = x * x
                return z
            def calcu_1_2k(a, b, c):
                x1 = (-(b) + math.sqrt(square(b) - 4 * (a * c))) / (2 * a)
                x2 = (-(b) - math.sqrt(square(b) - 4 * (a * c))) / (2 * a)
                return [x1, x2]
            print((calcu_1_2k(5, -4, -1)))

    输出结果：

    .. code-block:: python

        [1.0, -0.2]     # 一元二次方程 5x²-4x-1=0 的解

3.返回（条件返回）
-----------------

.. image:: images/11Functions/function_return.png
3.1 描述
++++
    返回语句会将程序或者函数的处理结果返回到原来调用的地方，并把程序的控制权一起交回。

    Mixly中的返回模块在有返回值的函数中可以连接返回值，在无返回值的函数中不可以。

.. attention::
    * 函数执行中遇到第一个返回语句时，就会结束函数的执行，不会运行其他的返回语句；
    * 条件返回让你可以直接将条件语句连接在返回模块中，仅适用于条件返回的情况。

3.2 示例
++++

    下面定义了一个函数welcome，如果传入的参数name的名字在bad_guys列表里，则直接返回，不会再执行输出"欢迎使用Mixly！"的语句。

    .. image:: images/11Functions/function_return_example.png

    .. image:: images/11Functions/function_return_example2.png

    源代码：

    .. code-block:: python
            :linenos:

            def welcome(name):
                bad_guys = ["Sam", "Tom", "Mike"]
                print(("你好，" + name))
                if name in bad_guys:
                    return None
                print("欢迎使用Mixly！")
            welcome("Fred")
            welcome("Tom")


    输出结果：

    .. code-block:: python

        你好，Fred
        欢迎使用Mixly！
        你好，Tom
