显示器
=================

.. image:: ../images/16Sensor2/Monitor/monitor.png

待完善~

.. image:: ../images/16Sensor2/Monitor/disp.png

1. IIC初始化
-------------

.. code-block:: python
        :linenos:

        import board
        import busio
        import tm1650


        i2c_extend = busio.I2C(scl=board.IO1, sda=board.IO2, frequency=100000)
        disp = tm1650.TM1650(i2c_extend)   


1.1 描述
++++++++

IIC（Inter-Integrated Circuit）其实是IICBus简称，所以中文应该叫集成电路总线，它是一种串行通信总线。I2C串行总线一般有两根信号线，一根是双向的数据线SDA，另一根是时钟线SCL，所有接到I2C总线设备上的串行数据SDA都接到总线的SDA上，各设备的时钟线SCL接到总线的SCL上。

使用IIC通信协议时可在通信线路上同时挂载多个设备，因此这里介绍的I2C指令即用于多个设备与主控器间的数据传输，关于IIC更多信息，请自行网上搜索下。
I2C初始化及使用I2C条指令需同时使用，首先，I2C初始化在实际使用中须关注SCL和SDA的两个参数的设置需要根据所连接的扩展口进行设置，例如，这里连接U1口时，SCL设置为1，SDA设置为2,注意顺序，先小后大；至于初始化后的实例名及频率一般默认即可。
其次，使用I2C时将上面初始化的实例名，并根据实际使用自定义改变“i2c_extend”这个字符串初始化为对应的传感器型号，以防冲突。

* 综上，需要注意的点有：①SCL、SDA的设置顺序，②i2c_extend重新命名 尤其使用多个数码管模块时
* 以上两点错一个地方，程序都无法正常运行

2. 四位数码管状态
----------------------

.. image:: ../images/16Sensor2/Monitor/disp_on.png

2.1描述
++++++++

根据使用需要，选择开、关、清屏等相关操作参数。

3. 四位数码管显示数字
----------------------

.. image:: ../images/16Sensor2/Monitor/disp_show.png

3.1 描述
+++++++++
用于显示数字

4. 四位数码管某个小数点状态控制
--------------------------------

.. image:: ../images/16Sensor2/Monitor/disp_dot.png

4.1 描述
+++++++++
设置某个位置的小数点点亮或熄灭

5. 四位数码管亮度控制
--------------------------------

.. image:: ../images/16Sensor2/Monitor/disp_light.png

5.1 描述
+++++++++

设置数码管亮度

范例
++++++++

将四位数码管连接至U1（SCL:1,SDA:2）,显示888.8

.. image:: ../images/16Sensor2/Monitor/example.png