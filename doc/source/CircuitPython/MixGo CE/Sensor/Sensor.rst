传感器
=================


外接传感器类别中包括了I2C初始化、光线传感器LTR308、气压传感器HP203B、温湿度传感器SHTC3、温湿度传感器AHT21、激光测距仪VL53L0X、SPI初始化、RFID读卡 读取卡号、RFID读卡 读取内容、RFID写卡 写入数据为、风火轮 触摸传感器、风火轮 触摸传感器 获取角度、超声波测距、初始化天气传感器、天气传感器 获取风向、天气传感器 获取风速、天气传感器 获取x秒内的降雨量等指令。

.. image:: ../images/16Sensor2/Sensor/sensor.png

1. IIC初始化
-----------------------

.. image:: ../images/16Sensor2/Sensor/iic.png

.. code-block:: python
        :linenos:

        import board
        import busio
        import ltr308al


        i2c_extend = busio.I2C(scl=board.IO1, sda=board.IO2, frequency=100000)
        xsensor1 = ltr308al.LTR_308ALS(i2c_extend)


1.1 描述
++++++++
IIC（Inter-Integrated Circuit）其实是IICBus简称，所以中文应该叫集成电路总线，它是一种串行通信总线。I2C串行总线一般有两根信号线，一根是双向的数据线SDA，另一根是时钟线SCL，所有接到I2C总线设备上的串行数据SDA都接到总线的SDA上，各设备的时钟线SCL接到总线的SCL上。

使用IIC通信协议时可在通信线路上同时挂载多个设备，因此这里介绍的I2C指令即用于多个设备与主控器间的数据传输，关于IIC更多信息，请自行网上搜索下。
I2C初始化及使用I2C条指令需同时使用，首先，I2C初始化在实际使用中须关注SCL和SDA的两个参数的设置需要根据所连接的扩展口进行设置，例如，这里连接U1口时，SCL设置为1，SDA设置为2,注意顺序，先小后大；至于初始化后的实例名及频率一般默认即可。
其次，使用I2C时将上面初始化的实例名，并根据实际使用自定义改变“xsensor”这个字符串初始化为对应的传感器型号，以防冲突。

* 综上，需要注意的点有：①SCL、SDA的设置顺序，②xsensor重新命名 尤其使用多种传感器时，③选对目标传感器型号
* 以上三点错一个地方，程序都无法正常运行

.. image:: ../images/16Sensor2/Sensor/sensor_iics.png

以上光线传感器LTR308、气压传感器HP203B、温湿度传感器SHTC3、温湿度传感器AHT21、激光测距仪VL53L0X五种传感器的使用方式相同，都是在IIC初始化为对应的传感器后进行使用。
对于气压传感器HP203B、温湿度传感器SHTC3、温湿度传感器AHT21三种传感器，可根据使用需要进行进一步参数设置，直接单击三角按钮选择即可，此处不再赘述。

1.2 范例1
++++++++++++

使用IIC通信连接外接一个光线传感器，以获取当前环境光照强度并通过串口每隔1秒打印一次，光线传感器连接至U4,IO11和IO12扩展接口上。


.. image:: ../images/16Sensor2/Sensor/ltr1.png

    .. code-block:: python
            :linenos:

            import board
            import busio
            import ltr308al
            import time


            i2c_extend = busio.I2C(scl=board.IO11, sda=board.IO12, frequency=100000)
            ltr1 = ltr308al.LTR_308ALS(i2c_extend)
            while True:
                print(ltr1.getdata())
                time.sleep(1)

* 根据上面图形代码示例可以看到，②和③所设置及选定的字符，在获取光线传感器数值指令中是对应一致才能正常使用。
* 此外，该范例将xsensor自定义为ltr1,也就是说可在同一主控上接多个相同的传感器。

1.3 范例2
++++++++++++

使用IIC通信连接外接两个光线传感器，以获取当前环境光照强度并通过串口每隔1秒打印一次，其中一个光线传感器连接至U4,IO11和IO12扩展接口上、另一个光线传感器连接至U1,IO1和IO2扩展接口上。

.. image:: ../images/16Sensor2/Sensor/ltr22.png

    .. code-block:: python
            :linenos:

            import board
            import busio
            import ltr308al
            import time


            i2c_extend = busio.I2C(scl=board.IO11, sda=board.IO12, frequency=100000)
            ltr1 = ltr308al.LTR_308ALS(i2c_extend)
            i2c_extend = busio.I2C(scl=board.IO1, sda=board.IO2, frequency=100000)
            ltr2 = ltr308al.LTR_308ALS(i2c_extend)
            while True:
            print('ltr1:', ltr1.getdata())
            print('ltr2:', ltr2.getdata())
            time.sleep(1)

1.3 范例3
++++++++++++

使用IIC通信连接外接一个光线传感器和一个气压传感器，以获取当前环境光照强度及气压强度值并通过串口每隔1秒打印一次，其中一个光线传感器连接至U4,IO11和IO12扩展接口上、另一个气压传感器连接至U1,IO1和IO2扩展接口上。

.. image:: ../images/16Sensor2/Sensor/ltr_hp.png

    .. code-block:: python
            :linenos:

            import board
            import busio
            import ltr308al
            import hp203x
            import time


            i2c_extend = busio.I2C(scl=board.IO11, sda=board.IO12, frequency=100000)
            ltr1 = ltr308al.LTR_308ALS(i2c_extend)
            i2c_extend = busio.I2C(scl=board.IO1, sda=board.IO2, frequency=100000)
            hp1 = hp203x.HP203X(i2c_extend)
            while True:
                print('ltr1:', ltr1.getdata())
                print('hp1:', hp1.p_data())
                time.sleep(1)

温湿度传感器SHTC3、温湿度传感器AHT21、激光测距仪VL53L0X三种传感器的用法与上相同，此处不再多述。

2. SPI初始化
---------------

.. image:: ../images/16Sensor2/Sensor/spi.png

SPI是串行外设接口（Serial Peripheral Interface）的缩写，是一种高速的，全双工，同步的通信总线，并且在芯片的管脚上只占用四根线，分别为：SCK、MOSI、MISO、CS,更多信息请网上自查。
* 在使用时，这里着重关注SCK、MOSI、MISO、CS四个管脚的设置即可，下面将以RFID外接传感器为例进一步说明SPI的使用。
射频识别（RFID）是 Radio Frequency Identification 的缩写，无线射频识别即射频识别技术（Radio Frequency Identification，RFID），是自动识别技术的一种，通过无线射频方式进行非接触双向数据通信，利用无线射频方式对记录媒体（电子标签或射频卡）进行读写，从而达到识别目标和数据交换的目的，更多信息自行网搜。

2.1 范例1
++++++++++++

通过串口打印RFID所读取到的卡号。将RFID连接至U1扩展口，此时将SCK、MOSI、MISO、CS四个脚对应设置为1、2、4、5即可，亦可连接至U2（6、7、9、10）。
* 以上内容针对MixGo CE v3.5及以上版本，对于V3.5版本以下的RFID使用直接基于图形块上的管脚使用即可，无需修改管脚即可直接使用5、6、7、35。

.. image:: ../images/16Sensor2/Sensor/spi_rf.png

.. code-block:: python
        :linenos:

        from board import *
        import busio
        import rc522
        import digitalio
        import time


        spi = busio.SPI(clock=IO1, MOSI=IO2, MISO=IO4)
        rfid = rc522.RC522(spi,digitalio.DigitalInOut(IO5))
        while True:
            print('rfid:', rfid.read_card(0, x="id"))
            time.sleep(1)
2.2 范例1
++++++++++++

读取并打印RFC卡某个位置的内容，一般情况下，新买的卡是空卡，所以读取内容之前需要先写入内容，内容存储位置大小，不同卡存在一定差异，根据需要从0位置逐步往上累积存储即可，当状态栏出现“Out of address range”内容时，即已超出可用最大位置。

.. image:: ../images/16Sensor2/Sensor/nfc_1.png

.. code-block:: python
        :linenos:
        from board import *
        import busio
        import rc522
        import digitalio
        import time


        spi = busio.SPI(clock=IO1, MOSI=IO2, MISO=IO4)
        rfid = rc522.RC522(spi,digitalio.DigitalInOut(IO5))
        rfid.write_card('Mixly',0)
        while True:
            print('rfid:', rfid.read_card(0, x="content"))
            time.sleep(1)

* 写入和读取到的内容格式都为字符串格式，需要注意这点。

.. Attention::
    超出存储位置报错详情：
	File "rc522.py", line 247, in write_card
IndexError: Out of address range


3. 风火轮 触摸传感器
---------------------

该传感器位于3.5版本以下的扩展板上（待测试）。

.. image:: ../images/16Sensor2/Sensor/fhl.png

3. 超声波测距
---------------

.. image:: ../images/16Sensor2/Sensor/distance.png

由于超声波指向性强，能量消耗缓慢，在介质中传播的距离较远，因而超声波经常用于距离的测量，更多信息自行网搜。

3.1 范例
++++++++++++++

将超声波模块连接至U4扩展口，通过串口每秒打印输出一次超声波测量到的距离数据。

.. image:: ../images/16Sensor2/Sensor/distance_1.png

.. code-block:: python
        :linenos:

        import hcsr04
        from board import *
        import time

        sonar = hcsr04.HCSR04(trigger_pin=IO11, echo_pin=IO12)


        while True:
            print('distance:', sonar.distance())
            time.sleep(1)

* 这里连接的是U4扩展口，因此设置tri为IO11、echo为IO12，连接至其他口时，自行修改即可。

4.天气信息获取
---------------

该模块指令待测试完善~

.. image:: ../images/16Sensor2/Sensor/weather.png
