板载传感
=================
传感模块：按钮、触摸传感器、红外接近传感器、光线传感器、声音传感器、板载温度和CPU温度、加速度、RTC时钟等

.. image:: images/13Sensor/sensor.png

在传感器部分，我们将一些最常见的传感器相关指令进行封装，这大大节省了学生的编程时间，这将更有助于使学生将更多的精力集中在创意实践上。

1.按钮
-----------------------

.. image:: images/13Sensor/button.png

.. code-block:: python
        :linenos:

        from button import button_B1


        button_B1.is_pressed()
        button_B1.was_pressed()
        button_B1.get_presses(1)

1.1 描述
++++++++++++

按钮状态监测的三种情况：①按钮被按着？只要按着则返回True,一直按着则一直返回True,不按就返回False;②按钮被按下？是一个完整动作，也就是按下后被检测到了返回True,若不松开，则在此期间返回None，仅当松开后返回一次False，之后一直返回None;③至于按钮在某个时长内被按下的次数，某个时间段内一次不按则返回0，按几次返回数值几。

1.2 范例
++++++++++++

通过串口打印输出三种按钮检测情况的返回值（若实际测试中看着不方便，可以逐一测试）。

如:

    .. image:: images/13Sensor/button_example.png

    源代码：

    .. code-block:: python
            :linenos:

            import time
            from button import button_B1
            from button import button_B2
            from button import button_A1


            while True:
                time.sleep(1)
                print('被按下返回：', button_B1.was_pressed())
                print('被按着返回：', button_B2.is_pressed())
                print('被按下的次数返回：', button_A1.get_presses(1))

2.触摸传感器
-----------------------

.. image:: images/13Sensor/touch.png

.. code-block:: python
        :linenos:

        from touchpad import touch_T1


        touch_T1.is_touched()

2.1 描述
++++++++++++

MixGo CE板载4个触摸按键，返回值为布尔值，即当触摸按键时返回True,否则返回False。

2.2范例
++++++++++++

通过串口返回触摸传感器的状态。

.. image:: images/13Sensor/touch_example.png

    .. code-block:: python
        :linenos:

        from touchpad import touch_T1
        import time


        while True:
            print(touch_T1.is_touched())
            time.sleep(1)
            
使用串口输出测试时，一般加个延时缓冲下，否则打印输出太快不利观察，也易卡顿。

3.红外接近传感器
-----------------------

.. image:: images/13Sensor/near.png

.. code-block:: python
        :linenos:

        from infrared import near


        near("left")

3.1 描述
++++++++++++

MixGo CE 3.0版本以上，带有两对红外接近传感器，用于检测是否有人体靠近；返回值为布尔值，当人体靠近传感器时返回True,否则返回为False。

3.2范例
++++++++++++

通过串口返回左侧的红外接近传感器的状态。

.. image:: images/13Sensor/near_example.png

    .. code-block:: python
        :linenos:

        import time
        from infrared import near


        while True:
            time.sleep(1)
            print(near("left"))

4.光线传感器
-----------------------

.. image:: images/13Sensor/light.png

.. code-block:: python
        :linenos:

        import sensor


        sensor.get_brightness()

4.1 描述
++++++++++++

MixGo CE 板载光线传感器，用于检测当前所在环境的光照强度，返回值为模拟量，范围0-65535。

4.2范例
++++++++++++

通过串口返回当前环境的光照强度值。

.. image:: images/13Sensor/light_example.png

    .. code-block:: python
        :linenos:

        import time
        import sensor


        while True:
            time.sleep(1)
            print(sensor.get_brightness())

5.声音传感器
-----------------------

.. image:: images/13Sensor/sound.png

.. code-block:: python
        :linenos:

        import sensor


        sensor.get_soundlevel()

5.1 描述
++++++++++++

MixGo CE 板载声音传感器，用于检测当前所在环境的声音强度，返回值为模拟量，范围0-65535。

5.2范例
++++++++++++

通过串口返回当前环境的声音强度值。

.. image:: images/13Sensor/sound_example.png

    .. code-block:: python
        :linenos:

        import time
        import sensor


        while True:
            time.sleep(1)
            print(sensor.get_soundlevel())

6.温度传感器
-----------------------

.. image:: images/13Sensor/temperature.png

.. code-block:: python
        :linenos:

        import sensor


        sensor.get_temperature()

6.1 描述
++++++++++++

MixGo CE 板载温度传感器，用于检测当前所在环境的温度及CPU温度，CPU温度获取可通过点击该图形指令右下角三角按钮切换，返回值为模拟量，范围0-65535。

6.2范例
++++++++++++

通过串口返回当前环境的温度值。

.. image:: images/13Sensor/temperature_example.png

    .. code-block:: python
        :linenos:

        import time
        import sensor


        while True:
            time.sleep(1)
            print('板载：', sensor.get_temperature())

7.加速度传感器
-----------------------

.. image:: images/13Sensor/acceleration.png

.. code-block:: python
        :linenos:

        from mixgoce import acc


        acc.acceleration[0]

7.1 描述
++++++++++++

MixGo CE 板载三轴加速度传感器，用于检测控制板当前的状态，具体可细分为x轴、y轴、z轴，返回值为浮点数。

7.2范例
++++++++++++

通过串口返回控制板x轴加速度值。

.. image:: images/13Sensor/acceleration_example.png

    .. code-block:: python
        :linenos:

        import time
        from mixgoce import acc


        while True:
            time.sleep(1)
            print('x轴：', acc.acceleration[0])

8.设置RTC时钟
-----------------------

.. image:: images/13Sensor/rtc_clock.png

.. code-block:: python
        :linenos:

        from mixgoce import rtc_clock
        import time


        rtc_clock.datetime = time.struct_time((2020,12,26,14,20,45,6,361,-1))

        rtc_clock.datetime

8.1 描述
++++++++++++

实时时钟的缩写是RTC(Real_Time Clock)，其中time.struct_time((2020,12,26,14,20,45,6,361,-1))为初始化操作，之后通过rtc_clock.datetime指令获取当前时间。

8.2范例
++++++++++++

通过串口每隔1秒打印一次当前时间。

.. image:: images/13Sensor/rtc_clock_example.png

    .. code-block:: python
        :linenos:

        from mixgoce import rtc_clock
        import time


        rtc_clock.datetime = time.struct_time((2020,12,26,14,20,45,6,361,-1))
        while True:
            time.sleep(1)
            print(rtc_clock.datetime)