串口
===================

串行接口是一种可以将接收来自CPU的并行数据字符转换为连续的串行数据流发送出去，同时可将接收的串行数据流转换为并行的数据字符供给CPU的器件。一般完成这种功能的电路，我们称为串行接口电路。
串口模块类别中包括了输出函数print()、获取键盘输入函数input()、硬件串口输入输出函数ai_com.uart_rx()、uart1.write()、ai_com.uart_tx()等相应调试接口函数控制语句。

.. image:: images/12Serial/serial.png

1.打印自动换行
-----------------------

.. image:: images/12Serial/println.png

.. code-block:: python
        :linenos:

        print('Mixly')

1.1 描述
++++++++++++

print() 方法用于打印输出，最常见的一个函数。

1.2 范例1
++++++++++++

打印输出“Hello Mixly!”，并自动换行。

如:

    .. image:: images/12Serial/println_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print('Hello Mixly!')


2.打印
---------------

.. image:: images/12Serial/print.png
    
源代码:

.. code-block:: python
        :linenos:

        print('Mixly',end ="")

2.1 描述
++++++++

打印，不换行。

2.2 参数
++++++++

end为空时，打印内容不自动换行。

2.3 范例
++++++++

    .. image:: images/12Serial/print_example.png

多次打印内容时会显示在同一行。

.. code-block:: python
        :linenos:
            
        print('Hello',end ="")
        print('Mixly',end ="")

3.打印结尾于
---------------

.. image:: images/12Serial/print_with.png

源代码:

.. code-block:: python
        :linenos:

        print('Mixly',end =',')


3.1 描述
++++++++++++++

打印某项内容，统一以xxx结尾。

3.2 参数
+++++++++++++++
* ，：结尾内容，可自定义。

3.3 范例
+++++++++

统一以字符串“end”结尾。

.. image:: images/12Serial/print_wi.png

.. code-block:: python
        :linenos:

        print('Mixly',end ='end')

4.打印（自动换行）2
--------------------

.. image:: images/12Serial/print_example2.png

.. code-block:: python
        :linenos:

        print('Hello', 'Mixly')

4.1 描述
++++++++++++++

将两个字符串连接后进行打印并换行，此处输出结果为“Hello Mixly”。

5.接收字符串输入
-----------------

.. image:: images/12Serial/input.png

.. code-block:: c
        :linenos:

        input('请输入:')

5.1 描述
++++++++++++++

input() 内置函数从标准输入读入一行文本，默认的标准输入是键盘。

5.2 范例
+++++++++

输入测试

.. image:: images/12Serial/input_example.png

.. code-block:: c
        :linenos:

        strr = input('请输入:')
        print('输入的内容为:', strr)

程序正常运行情况下

.. code-block:: c
        :linenos:

        请输入:Mixly
        输入的内容为:Mixly

PS:目前程序好像无法正常输入并接收数据，待完善

6.初始化串口
---------------

.. image:: images/12Serial/uart.png

.. code-block:: c
        :linenos:

        import busio
        from board import *


        uart1 = busio.UART(IO9, IO10, baudrate=115200)
        uart1.write(bytearray('Mixly'))


6.1 描述
++++++++++++++

MixGo CE可初始化出三个串口进行串口通信，分别为：UART1、UART2和 UART3。


6.2 参数
+++++++++++++++

* IO9, IO10, baudrate=115200 ：IO9, IO10分别对应为TX,RX；baudrate是指波特率，表示单位时间内传送的码元符号的个数，它是对符号传输速率的一种度量，常用的值为9600和115200。uart1.write(bytearray(''))为串口打印输出函数。
* 注：TX是发送（transport），RX是接收(receive)。

7.uart1串口打印自动换行
-------------------------

.. image:: images/12Serial/uart_w.png

.. code-block:: c
        :linenos:

        import busio
        from board import *


        uart1.write(bytearray('Mixly')+'\r\n')


7.1 描述
++++++++++++++

uart1.write(bytearray('')+'\r\n')方法用于uart1打印输出与其他硬件间进行串口通信，此函数需配合6.串口初始化进行使用；另外这条串口输出内容将不在软件状态栏显示。

7.2 范例
+++++++++

通信打印输出“Hello World”

.. image:: images/12Serial/uart_w3.png

.. code-block:: python
        :linenos:

        import busio
        from board import *


        uart1.write(bytearray(oct(0XFF00FF))+'\r\n')

        uart1 = busio.UART(IO9, IO10, baudrate=115200)
        uart1.write(bytearray('Hello World')+'\r\n')


8. uart1串口打印自动换行2
---------------------------

.. image:: images/12Serial/uart_w2.png

.. code-block:: python
        :linenos:

        import busio
        from board import *


        uart1.write(bytearray(hex(0XFF00FF))+'\r\n')

8.1 描述
++++++++++++++

uart1根据实际应用需要打印输出二、八、十、十六进制数据。

8.2 参数
+++++++++++++++

* hex()此处为16进制数据转换方法，可替换为bin()二进制数据转换、oct()八进制数据转换、int()十进制数据转换;uart1.write(bytearray(hex())+'\r\n')函数同样需要配合6.串口初始化进行使用，范例参见7.2。


9.uart1发送数据至MixGo AI
----------------------------

.. image:: images/12Serial/uart_ai.png

.. code-block:: python
        :linenos:

        import busio
        from board import *
        import ai_com


        uart1 = busio.UART(IO9, IO10, baudrate=115200)
        ai_com.uart_tx(uart1, code, repeat=1)


9.1 描述
++++++++++++++

uart1串口发送数据至MixGo AI板专用函数。

9.2 参数
+++++++++++++++

* uart1为串口实例化对象，code为要发送的数据存储变量名，repeat为是否重复发送某一数据的标记变量，当该值等于1时会重复发送code中的变量值，当为0时，仅当code中数据发生改变时才会被发送一次。
* 尤其注意code是要发送的数据变量名，需要遵循先定义后使用原则。

9.2 范例
++++++++++++++

通过uart持续发送数据1至MixGo AI板。

MixGo CE端程序:

.. image:: images/12Serial/uart_ai1.png

.. code-block:: python
        :linenos:

        import busio
        from board import *
        import ai_com


        code = 1
        uart1 = busio.UART(IO9, IO10, baudrate=115200)
        ai_com.uart_tx(uart1, code, repeat=1)

MixGo AI端程序:

.. image:: images/12Serial/uart_ai2.png

.. code-block:: python
        :linenos:

        from machine import UART
        import board
        import machine
        import ce_com


        board.register(7,board.FPIOA.UART1_TX)
        board.register(6,board.FPIOA.UART1_RX)
        uart1=UART(UART.UART1, 115200, timeout=1000, read_buf_len=4096)
        while True:
            接收 = ce_com.uart_rx(uart1)
            if 接收 != None:
                print('接收到：', 接收)

10.读取MixGo AI数据
---------------------------

.. image:: images/12Serial/ai_read.png

.. code-block:: python
        :linenos:

        import busio
        from board import *
        import ai_com


        uart1 = busio.UART(IO9, IO10, baudrate=115200)
        res = ai_com.uart_rx(uart1)

10.1 描述
++++++++++++++

MixGo CE从串口1读取MixGo AI数据。

10.2 返回
+++++++++++++++
返回MixGo AI传输过来的数据。

10.3 范例
+++++++++

接收MixGo AI发送过来的数据。

MixGo CE端代码

.. image:: images/12Serial/ai_readexample.png

.. code-block:: c
        :linenos:

        import busio
        from board import *
        import ai_com


        uart1 = busio.UART(IO9, IO10, baudrate=115200)
        while True:
            res = ai_com.uart_rx(uart1)
            if res != None:
                print('收到:', res)

MixGo AI端代码

.. image:: images/12Serial/ai_send.png

.. code-block:: python
        :linenos:

        from machine import UART
        import board
        import time
        import machine
        import ce_com


        board.register(7,board.FPIOA.UART1_TX)
        board.register(6,board.FPIOA.UART1_RX)
        uart1=UART(UART.UART1, 115200, timeout=1000, read_buf_len=4096)
        code = 0
        while True:
            time.sleep_ms(1000)
            ce_com.uart_tx(uart1, code, repeat=1)

11.待验证
---------------------

.. image:: images/12Serial/fun.png