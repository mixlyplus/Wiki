板载显示
===================

板载显示模块：显示(图像/字符串/动画)、滚动显示字符串、显示(图像/字符串/动画)间隔、滚动显示字符串、创建图像、内置图像、整体移动、判断某个位置的LED灯是否亮着、设置某个坐标的LED灯状态、设置LED点阵屏亮度、获取点阵屏亮度、清楚显示内容等指令。

.. image:: images/15Monitor/monitor.png

1.1 显示(图像/字符串/动画)
------------------------------

通过板载LED点阵屏显示内置图像/字符串/动画

.. image:: images/15Monitor/display_1.png

.. code-block:: python
        :linenos:

        from mmatrix import display
        import Image


        display.show_dynamic(Image.HEART)

* 字符串这里是动态跳动逐一字符显示，具体可自行测试查看，不支持中文字符显示。

1.2 滚动显示字符串
-----------------------

从左至右滚动显示字符串。

.. image:: images/15Monitor/display_2.png

.. code-block:: python
        :linenos:

        from mmatrix import display


        display.scroll('Mixly')

1.3 静态显示字符串
-----------------------

静态显示字符串，注意和1.1显示字符串的效果相区别。

.. image:: images/15Monitor/display_3.png

.. code-block:: python
        :linenos:
        
        from mmatrix import display


        display.show_static('abc')

1.4 显示(图像/字符串/动画) 间隔x毫秒
--------------------------------------

由于所显示的内容为一次性显示出来，所以与1.1的区别并不显著。

.. image:: images/15Monitor/display_4.png

.. code-block:: python
        :linenos:
        
        from mmatrix import display
        import Image


        display.show_dynamic(Image.HEART,120)

1.5 滚动显示字符串 间隔x毫秒
-------------------------------

* 间隔x毫秒，这里的间隔指字符串每向左移动一次所间隔的时间。
  
.. image:: images/15Monitor/display_5.png

.. code-block:: python
        :linenos:
        
        from mmatrix import display


        display.scroll('Mixly',1000)

1.6 创建图像
---------------

通过点击黑色矩形块，该指令可以快速创建出非内置图像，创建出的内容可以用于1.1和1.4指令。

.. image:: images/15Monitor/display_6.png

.. code-block:: python
        :linenos:
        
        bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')

1.6.1 范例
++++++++++++

创建图像并显示。

.. image:: images/15Monitor/display_66.png

.. code-block:: python
        :linenos:

        from mmatrix import display


        display.show_dynamic(bytearray(b'\x00\x00\x00\x00\x00\x18\x18\xfe\x01\xfe\x18\x18\x00\x00\x00\x00'))

1.7 内置图像
-----------------------

供1.1和1.4指令使用，使用范例略。

.. image:: images/15Monitor/display_7.png

.. code-block:: python
        :linenos:

        import Image


        Image.HEART

1.8 向(上/下/左/右)移动
------------------------------

该指令主要用于LED点阵屏当前所显示的内容的整体移动操作。

.. image:: images/15Monitor/display_8.png

.. code-block:: python
        :linenos:

        from mmatrix import display


        display.up(1)


1.8.1 范例
++++++++++++

通过1.1中的显示图像指令显示一个心形，后通过移动指令借助for循环，每隔1秒向上移动1次，8次后将心形移出显示屏。

.. image:: images/15Monitor/display_88.png

    .. code-block:: python
        :linenos:

        from mmatrix import display
        import Image
        import time


        display.show_dynamic(Image.HEART)
        for i in range(0, 8, 1):
            display.up(1)
            time.sleep(1)

1.9 检测某坐标LED灯是否亮着
---------------------------------

.. image:: images/15Monitor/display_9.png


.. code-block:: python
        :linenos:

        from mmatrix import display


        display.get_pixel(int(0), int(0))

1.9.1 返回
++++++++++++++++++

当所检测的坐标对应的LED状态为亮，则返回1，否则返回0，范例略。

1.10 设置亮灭
---------------

点亮或熄灭某坐标的LED灯。

.. image:: images/15Monitor/display_10.png

.. code-block:: python
        :linenos:

        from mmatrix import display


        print(display.get_pixel(int(0), int(0)))

        display.set_pixel(int(0), int(0), 1)

1.11 设置亮度
------------------

.. image:: images/15Monitor/display_11.png

.. code-block:: python
        :linenos:

        from mmatrix import display


        display.set_brightness(0)

在LED点阵屏有显示内容的情况下，再使用该指令可进行设置点阵屏的亮度。

1.11.1 范例
++++++++++++

点阵屏在显示内容情况下，设置显示亮度（目前测试的效果是仅0会变暗，其他选项区别不大，待进一步核实）。

.. image:: images/15Monitor/display_111.png

    .. code-block:: python
        :linenos:

        from mmatrix import display
        import Image


        display.show_dynamic(Image.HEART)
        display.set_brightness(0)

* 注意是先有显示内容，再设置亮度。
  
.. image:: images/15Monitor/display_112.png

* 该指令用于返回当前点阵屏亮度

1.12 清除显示内容
-----------------------

.. image:: images/15Monitor/display_12.png

.. code-block:: python
        :linenos:

        from mmatrix import display


        display.clear()

清空点阵屏当前的显示内容，范例略。
