
软件使用基础
---------------------------------------
.. toctree::
   :maxdepth: 2

   basic/index.rst

Arduino AVR 编程
-------------------
.. toctree::
   :maxdepth: 3

   Arduino/index.rst

MicroPython 编程
-----------------------
.. toctree::
   :maxdepth: 2

   MicroPython/index.rst

Python 编程
-------------------
.. toctree::
   :maxdepth: 2

   Python/index.rst

CircuitPython 编程
-----------------------
.. toctree::
   :maxdepth: 2

   CircuitPython/index.rst