文本
===============
文本的具体内容为代码的文本输出功能，具体包括：文本连接、文本转数字、数字转文本。

.. image:: images/05Text/text.png

所有文本内容可通过串口监视器或外接显示屏打印。

字符串
------------

.. image:: images/05Text/string.png
.. code-block:: c
    :linenos:

    "hello";

描述
++++++++++++++

	字符串常量。

参数
+++++++++++++++
* 字符串: 需要输入的字符串内容。

字符
-------------

.. image:: images/05Text/char.png
.. code-block:: c
    :linenos:

    'a';

描述
++++++++++++++

	字符常量。

参数
+++++++++++++++
* 字符: 需要输入的字符。

范例
+++++++++

	只执行一次串口输出积木块。

.. image:: images/01Input-Output/analogRead-example.png

.. code-block:: c
    :linenos:

	void setup(){
	  Serial.begin(9600);
	}
	void loop(){
	  Serial.println(analogRead(A0));
	}

.. Attention::
	只能输入一个字符，输入超过一个字符时会自动选择第一个字符进行输入，空格也可以作为字符输入。

字符串连接
-------------

.. image:: images/05Text/stringPlus.png
.. code-block:: c
    :linenos:

    String("Hello") + String("Mixly");

描述
++++++++++++++

	字符串连接，可以将两个或者多个字符串连接成一个。

范例
+++++++++

	连接多个字符串。

.. image:: images/05Text/stringPlus_example.png

.. code-block:: c
    :linenos:

    void loop(){
      String("Hello") + String(String("Hello") + String("Mixly"));
    }

字符串转数字(toInt()/toFloat())
--------------------------------

.. image:: images/05Text/stringtonum.png
.. code-block:: c
    :linenos:

    String("123").toInt();

描述
++++++++++++++

	将字符串中的数字转成整数或者小数。

获取字符串中的一部分
-------------------

.. image:: images/05Text/getPart.png
.. code-block:: c
    :linenos:

    String("substring").substring(0,3);

描述
++++++++++++++

	获取字符串第x到y的字符串。

范例
+++++++++

	截取字符串中的一部分。

.. image:: images/05Text/getPart_example.png

.. code-block:: c
    :linenos:

    void setup(){
      Serial.begin(9600);
    }
    void loop(){
      Serial.println(String("I Love Mixly").substring(7,12));
      while(true);
    }

小数位数保留
--------------

.. image:: images/05Text/keepFigures.png

.. code-block:: c
    :linenos:

	String(6.666, 2);

描述
++++++++++++++

	将小数按照指定位数进行化简。

参数
+++++++++++++++
* 小数:需要化简的小数。
* 位数:需要保留的位数。

转化大小写
--------------

.. image:: images/05Text/stringChange.png

.. code-block:: c
    :linenos:

	String.toUpperCase();

描述
++++++++++++++

	将指定的字符串变量的小写字母全部转化为大写字母或将大写字母全部转化为小写字母.

参数
+++++++++++++++
* 第一个字符串:需要转化的字符串变量。
* 转化方向：确定是将小写字母转化为大写字母还是将大写字母转化为小写字母。

范例
+++++++++

	将字符串变量里的小写字母全部转化为大写字母。

.. image:: images/05Text/stringChange_example.png

.. code-block:: c
    :linenos:

    String wenzi;
    void setup(){
      Serial.begin(9600);
      wenzi = " ";
    }
    void loop(){
      wenzi = " I like Mixly! ";
      Serial.println(wenzi);
      wenzi.toUpperCase();
      while(true);
    }

字符串变量替换
--------------

.. image:: images/05Text/stringReplace.png

.. code-block:: c
    :linenos:

	String.replace("s", "Q");

描述
++++++++++++++

	将指定的字符或字符串替换为指定的字符或字符串.

参数
+++++++++++++++
* 第一个字符串:原有的字符串。
* 第二个字符串:需要进行替换的字符或字符串。
* 第三个字符串:用来进行替换的字符或字符串。

范例
+++++++++

	将字符串变量a中的“l”全部替换为“a”。

.. image:: images/05Text/stringReplace_example.png

.. code-block:: c
    :linenos:

    String a;
    void setup(){
      a = "hello";
      Serial.begin(9600);
    }
    void loop(){
      a.replace("l", "a");
      Serial.println(a);
      while(true);
    }

消除非可视字符
--------------

.. image:: images/05Text/eliminate.png

.. code-block:: c
    :linenos:

	String.trim();

描述
++++++++++++++

	截取字符串中间的非空白字符.

参数
+++++++++++++++
* 字符串变量

范例
+++++++++

	消除非可视字符将删除字符串首尾的非可视字符。

.. image:: images/05Text/eliminate_example.png

.. code-block:: c
    :linenos:

    String wenzi;
    void setup(){
      Serial.begin(9600);
      wenzi = " ";
    }
    void loop(){
      wenzi = " I like Mixly! ";
      Serial.println(wenzi);
      wenzi.trim();
      Serial.println(wenzi);
      while(true);
    }

判断字符串的开头或结尾
--------------

.. image:: images/05Text/judgeString.png

.. code-block:: c
    :linenos:

	String("substring").startWith("substring");

描述
++++++++++++++

	判断第一个字符串是否以第二个字符串为开头或结尾，若是则返回1，否则返回0.

参数
+++++++++++++++
* 第一个字符串:需要进行判断的字符串。
* 第二个字符串:用来进行判断的字符串。

范例
+++++++++

	substring以sub开头而不以ub开头。

.. image:: images/05Text/judgeString_example.png

.. code-block:: c
    :linenos:

    void setup(){
      Serial.begin(9600);
    }
    void loop(){
      Serial.println(String("substring").startsWith("sub"));
      Serial.println(String("substring").startsWith("ub"));
      while(true);
    }

转ASCII字符(char())
--------------------

.. image:: images/05Text/numtochar.png
.. code-block:: c
    :linenos:

    char(223);

描述
++++++++++++++

	将数字转成ASCII字符。

参数
+++++++++++++++
* 数字: ASCII字符对应的数字。

返回
+++++++++
数字所对应的ASCII字符。

范例
+++++++++

	用转ASCII字符积木块加空格。

.. image:: images/01Input-Output/analogRead-example.png

.. code-block:: c
    :linenos:

	void setup(){
	  Serial.begin(9600);
	}
	void loop(){
	  Serial.println(analogRead(A0));
	}

转ASCII数值(toascii)
-----------------------

.. image:: images/05Text/toascii.png
.. code-block:: c
    :linenos:

    toascii('a');

描述
++++++++++++++

	将字符转成ASCII码。

参数
+++++++++++++++
* ASCII字符: ASCII码对应的字符。

返回
+++++++++
ASCII码

进制 转字符串
--------------------

.. image:: images/05Text/numtostring.png
.. code-block:: c
    :linenos:

    String(0, BIN);

描述
++++++++++++++

	将一个数字转化为字符串，支持二进制、八进制、十进制、十六进制数字。

参数
+++++++++++++++
* 数字: 对应进制的数字。

返回
+++++++++
	字符串类型的数字。

.. Attention::
	在某些需要将不同进制的整数类型转换为字符串类型的情况下进行使用。

获取字符串长度(String().length())
-----------------------------------

.. image:: images/05Text/stringlength.png
.. code-block:: c
    :linenos:

    String("hello").length();

描述
++++++++++++++

	通过String().length()方法获得字符串的长度，值为组成字符串的字母数。

参数
+++++++++++++++
* 字符串: 需要获取长度的字符串。

返回
+++++++++
	字符串的字符个数，一个中文算三个字符。

获取字符串第N个字符
-------------------

.. image:: images/05Text/charat.png
.. code-block:: c
    :linenos:

    String("hello").charAt(0);

描述
++++++++++++++

	获取到字符串的第N个字符。

参数
+++++++++++++++

* N: 获取的字符排位


字符串比较1
---------------

.. image:: images/05Text/equals.png
.. code-block:: c
    :linenos:

    String("").equals(String(""));

描述
++++++++++++++

	比较两个字符串是否相同；第一个字符串从左往右是否包含第二个字符串；第一个字符串从右往左是否包含第二个字符串。

参数
+++++++++++++++

* 第一个字符串

* 第二个字符串

字符串比较2
---------------

.. image:: images/05Text/compare.png
.. code-block:: c
    :linenos:

    String("").compareTo(String(""));

描述
++++++++++++++

	运用compareTo方法对两个字符串进行比较，从第一位开始比较，如果遇到不同的字符，则马上返回这两个字符的ASCII值的差值，返回值是int类型。

参数
+++++++++++++++

* 第一个字符串

* 第二个字符串
