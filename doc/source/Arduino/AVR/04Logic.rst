逻辑
==============
逻辑模块中的指令大多是逻辑运算处理内容，具体可分为：条件判断、逻辑运算、？运算。

.. image:: images/04Logic/logic1.png

比较运算符
-----------

.. image:: images/04Logic/comparisonOperators.png
.. code-block:: c
    :linenos:

    0 == 0;

描述
********

.. note::
	比较运算符适用于比较两个值的运算符号，用比较运算符比较两个值时，结果是一个逻辑值，不是TRUE就是FALSE。

等于(=)
**********

等于用在变量之间、变量和自变量之间以及其他类型的信息之间的比较，判断符号两侧的数据值是否相等，要求两侧数据类型相同时才能比较。

规则是：如果两个数据值相等，数据类型相同，则结果为TRUE，否则输出FALSE。

范例
+++++++++

1=1，返回True；1=“1”，不成立

.. image:: images/04Logic/equal_example.png

.. code-block:: c
    :linenos:

    void setup(){
      Serial.begin(9600);
      if (1 == 1) {
        Serial.print("True");
      }
    }
    void loop(){
      0 == "1";
      1;
    }

不相等(≠)
*************

不等于是等于符号的相反判断。

规则：符号两边的数据值相等时，返回FALSE，否则返回TRUE。

小于(＜)
*************

小于符号进行有顺序的比较，如果符号左边的数据值小于右边的数据值，则返回TRUE，否则返回FALSE。

小于等于(≤)
********************

小于等于符号与小于符号类似，多出一种返回TRUE的情况，如果符号左边的数据值小于等于右边的数据值，则返回TRUE，否则返回FALSE。

大于(>)
**********************

大于符号进行有顺序的比较，如果符号左边的数据值大于右边的数据值，则返回TRUE，否则返回FALSE。

大于等于(≥)
********************

大于等于符号与大于符号类似，多出一种返回TRUE的情况，如果符号左边的数据值大于等于右边的数据值，则返回TRUE，否则返回FALSE。

范例
+++++++++

在A0连接电位器，D9连接LED。转动电位器，大于600灯一直亮，在400和600之间，灯灭，小于400灯闪。

.. image:: images/04Logic/comparisonOperators_example.png

.. code-block:: c
    :linenos:

    void setup(){
      Serial.begin(9600);
      pinMode(9, OUTPUT);
    }
    void loop(){
      Serial.println(analogRead(A0));
      delay(1000);
      if (analogRead(A0) >= 600) {
    digitalWrite(9,HIGH);
      } else if (analogRead(A0) <= 400) {
        digitalWrite(9,HIGH);
        delay(200);
        digitalWrite(9,LOW);
        delay(200);
      } else {
        digitalWrite(9,LOW);
      }
    }

逻辑运算符
-----------

.. image:: images/04Logic/logicOperators.png
.. code-block:: c
    :linenos:

    false && false;

描述
********

.. note::
	逻辑运算符用于判定变量或值之间的逻辑。

且
**********

且的含义同逻辑运算符与，只有当符号两边的表达式均为真时，才被判断为真，否则为假。

或
*************

或也是逻辑运算符的一种，只有当两边的表达式均为假时，才被判断为假，否则为真。

范例
+++++++++

光控按钮灯:A0连接光线传感器，D4连接LED，D3连接按钮,当A0<50光线暗）并按钮被被下时，LED为高电平;否则，LED为低电电平

.. image:: images/04Logic/logicOperators_example.png

.. code-block:: c
    :linenos:

    void setup(){
      pinMode(3, INPUT);
      pinMode(4, OUTPUT);
    }
    void loop(){
      if (analogRead(A0) < 50 && digitalRead(3)) {
        digitalWrite(4,HIGH);
      } else {
        digitalWrite(4,LOW);
      }
    }

？：语句
-----------

.. image:: images/04Logic/？statement.png
.. code-block:: c
    :linenos:

    (0 > 0)?false:false;

描述
********

.. note::
	对于条件表达式b ? x : y，先计算条件b，然后进行判断。如果b的值为true，计算x的值，运算结果为x的值；否则，计算y的值，运算结果为y的值。一个条件表达式绝不会既计算x，又计算y。条件运算符是右结合的，也就是说，从右向左分组计算。例如，a ? b : c ? d : e将按a ? b : (c ? d : e)执行。

参数
********
* 判断条件：先计算判断条件的值
* x：当判断条件的值为true，运算结果为x的值
* y：当判断条件的值为false，运算结果为y的值

范例
+++++++++

声控灯：A0连接声音传感器，D2连接LED，当A0>50时，LED为高电平；否则，LED为低电平

.. image:: images/04Logic/？statement_example.png

.. code-block:: c
    :linenos:

    void setup(){
      Serial.begin(9600);
      pinMode(2, OUTPUT);
    }
    void loop(){
      Serial.println(analogRead(A0));
      digitalWrite(2,(analogRead(A0) > 50)?HIGH:LOW);
    }
