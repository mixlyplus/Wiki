数组
======================
数组是为了便于代码编写时所采用的一种数字块。具体包括：定义数组、取数组值、改数组值。

.. image:: images/06Lists/array.png

数组像一个大盒子，可以储存一定个数的数字（第一个指令）或字符串（第二个指令）。在课程中的音乐盒一课，我们就用到了数组。

.. image:: images/06Lists/array2.png



如上图，tonelist, musiclist, highlist, rhythmlist分别储存了歌曲《两只老虎》的基本频率（即哆来咪等七音的对应频率），频率变化（即两只老虎的简谱数字），每个音的音名CDEFGAB以及每个音的时值（长短）。
《两只老虎》共有三十二个音，因此使用了一个从1到32的循环语句以此读取第一个音的频率变化（简谱，并通过.. image:: images/06Lists/array3.png语句可读取简谱的基本频率），随后与获取highlist里的对应音高并按thythmlist里的时值进行相应延时。
该代码将《两只老虎》的指令放到了一个名为playmusic的函数内，并通过执行 playmusic调用该函数。关于函数的内容，我们将在下面函数部分具体详解。


创建mylist数组1
-------------

.. image:: images/06Lists/def_array1.png
.. code-block:: c
    :linenos:

    int mylist[]={0, 0, 0};

描述
++++++++++++++

	初始化一个数组。

参数
+++++++++++++++
* 数组类型: 选择创建的数组类型，包括整数、长整数、小数、字符、字节、char、字符串。
* 数组名称：为创建的数组输入一个名字。

范例
+++++++++

	创建一个CHINA字符串数组。

.. image:: images/06Lists/def_array1_example.png

.. code-block:: c
    :linenos:

    char CHINA[]={'C', 'H', 'I', 'N', 'A'};
    void setup(){
    }
    void loop(){
    }

创建mylist数组2
---------------
.. image:: images/06Lists/def_array2.png
.. code-block:: c
    :linenos:

    int mylist[3]={0,0,0};

描述
++++++++++++++

	初始化一个数组。

参数
+++++++++++++++
* 数组类型: 选择创建的数组类型，包括整数、长整数、小数、字符、字节、char、字符串。
* 数组名称：为创建的数组输入一个名字。
* 数组长度：指定数组的长度。

获取数组mylist长度
------------------
.. image:: images/06Lists/sizeof.png
.. code-block:: c
    :linenos:

    sizeof(mylist)/sizeof(mylist[0]);

描述
++++++++++++++

	获取数组的长度。

参数
+++++++++++++++
* 数组名称: 需要获取长度的数组名称。

范例
+++++++++

	获取CHINA数组的长度。

.. image:: images/06Lists/def_array2_example.png

.. code-block:: c
    :linenos:

    int CHINA[]={'C', 'H', 'I', 'N', 'A'};
    void setup(){
      Serial.begin(9600);
    }
    void loop(){
      Serial.println(sizeof(CHINA)/sizeof(CHINA[0]));
      while(true);
    }

获取数组mylist第N项
--------------
.. image:: images/06Lists/array_index.png
.. code-block:: c
    :linenos:

    mylist[(int)(N - 1)];

描述
++++++++++++++

	获取指定数组的第N项。

参数
+++++++++++++++
* 数组名称: 需要获取内容的数组名称。
* 变量N: 需要获取数组的第N项。

范例
+++++++++

	串口输出字符型数组元素。

.. image:: images/06Lists/array_index_example.png

.. code-block:: c
    :linenos:

    char CHINA[]={'C', 'H', 'I', 'N', 'A'};
    void setup(){
       Serial.begin(9600);
    }
    void loop(){
      for (int i = (1); i <= (sizeof(CHINA)/sizeof(CHINA[0])); i = i + (1)) {
        Serial.print(CHINA[(int)(i - 1)]);
      }
      while(true);
    }


数组mylist第N项赋值
--------------
.. image:: images/06Lists/array_index2.png
.. code-block:: c
    :linenos:

    mylist[(int)(N - 1)] = 0;

描述
++++++++++++++

	为指定数组的第N项赋予特定的值。

参数
+++++++++++++++
* 数组名称: 需要获取内容的数组名称。
* 变量N: 需要赋值数组的第N项。
* 赋值内容: 需要替换的内容。

范例
+++++++++

	串口输出字符型数组元素。

.. image:: images/06Lists/array_index2_example.png

.. code-block:: c
    :linenos:

    char CHINA[]={'C', 'H', 'I', 'N', 'A'};
    volatile int N;
    void setup(){
      N = 2;
      Serial.begin(9600);
    }
    void loop(){
      CHINA[(int)(N - 1)] = 'C';
      Serial.print(CHINA[(int)(N - 1)]);
      while(true);
    }

创建二维数组array
-------------

.. image:: images/06Lists/def_2dimension_array.png
.. code-block:: c
    :linenos:

    int array[2][2]={{0,0},{0,0}};

描述
++++++++++++++

	初始化一个二维数组。

参数
+++++++++++++++
* 数组类型: 选择创建的数组类型，包括整数、长整数、小数、字符、字节、char、字符串。
* 数组名称：为创建的数组输入一个名字。
* 数组行数
* 数组列数
* 字符串：创建二维数组的来源。

给二维数组第M行N列赋值
-------------

.. image:: images/06Lists/2array_index.png
.. code-block:: c
    :linenos:

    array[M-1][N-1]=0;

描述
++++++++++++++

	为数组第M行第N列赋值。

参数
+++++++++++++++
* 数组名称：为创建的数组输入一个名字。
* 数组行数
* 数组列数
* 赋值：要赋予的值。

获取数组array第M行第N列
-------------

.. image:: images/06Lists/2array_index2.png
.. code-block:: c
    :linenos:

    array[M-1][N-1];

描述
++++++++++++++

	获取数组array第M行第N列的内容。

参数
+++++++++++++++
* 数组名称：为创建的数组输入一个名字。
* 数组行数
* 数组列数

范例
+++++++++

	串口输出二维数组元素。

.. image:: images/06Lists/2array_index2_example.png

.. code-block:: c
    :linenos:

    int array[3][3]={{1,2,3},{4,5,6},{7,8,9}};
    void setup(){
      Serial.begin(9600);
    }
    void loop(){
      for (int i = 1; i <= 3; i = i + (1)) {
        for (int j = 1; j <= 3; j = j + (1)) {
          Serial.print(String(array[i-1][j-1]) + String(","));
          if ((long) (j) % (long) (3) == 0) {
            Serial.println("");
          }
        }
      }
      while(true);
    }