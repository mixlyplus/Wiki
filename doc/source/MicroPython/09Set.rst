集合
======================
.. note::
    集合（set）是一个无序的不重复元素序列。

    Mixly中有很多用于集合操作的模块，如下：

.. image:: images/09Set/sets.png

1.集合初始化
-----------------

.. image:: images/09Set/set_initial.png
1.1 描述
++++
    Mixly中有两种初始化集合的方式：

    * 第一种，你需要输入集合名称，将代表各种数据的模块连接到初始化模块上，你也可以点击蓝色齿轮增加元素数量。
    * 第二种，你也需要输入集合名称，然后直接在花括号中输入各种数据，各个元素使用英文逗号分隔即可。

    在使用集合初始化模块后，你可以在"变量"模块分类中根据你输入的集合名称找到代表此集合的模块。

.. attention::
    * 集合内部的各个元素是没有顺序的，所以直接输出集合，每一次的输出结果都是随机的。
    * 集合的各个元素类型不要求相同，可以是数字、布尔值和字符串。
    * 集合中不能存在相同元素，且集合中的元素必须是不可变类型（列表、集合等为可变类型）。
    * 集合中的字符串元素都是由引号包围的，若其他数据类型使用引号包围也将会被视为字符串类型。

1.2 示例
++++

    .. image:: images/09Set/set_initial_example.png

    源代码：

    .. code-block:: python
            :linenos:

            s1= {"Mixly", 1, True}
            s2= {"Mixly", 1.5, True}
            s3 = {"Mixly", 0, False}
            s4 = {"Mixly", 1, False}
            print(s1)
            print(s2)
            print(s3)
            print(s4)

    输出结果：

    .. code-block:: python

        {1, 'Mixly'}        # 因为在Python中1 = 1.0 = True，所以仅保留了1这个元素
        {1.5, True, 'Mixly'}
        {0, 'Mixly'}        # 因为在Python中0 = False，所以仅保留了0这个元素
        {False, 1, 'Mixly'}

2.获取集合长度
-----------------

.. image:: images/09Set/set_get_length.png
2.1 描述
++++
    此模块可以返回一个集合的长度（元素数量）。

.. attention::
    此模块返回的长度为int类型的数据格式。

2.2 示例
++++

    .. image:: images/09Set/set_get_length_example.png

    源代码：

    .. code-block:: python
            :linenos:

            s1= {"Mixly", 1, True}
            print(s1)
            print(len(s1))


    输出结果：

    .. code-block:: python

        {1, 'Mixly'}
        2

3.集合删除并获取元素
-----------------

.. image:: images/09Set/set_pop.png
3.1 描述
++++
    此模块可以在给定的集合中删除随机元素，并返回被删除的元素值。

3.2 示例
++++

    .. image:: images/09Set/set_pop_example.png

    源代码：

    .. code-block:: python
            :linenos:

            s1= {"Mixly", 1, "Mixpy", 2}
            print(s1)
            print(s1.pop())
            print(s1)

    输出结果：

    .. code-block:: python

        {'Mixly', 1, 2, 'Mixpy'}
        Mixly
        {1, 2, 'Mixpy'}

4.集合运算
-----------------

.. image:: images/09Set/set_operator.png
4.1 描述
++++
    此模块可以将给定的两个集合进行并集、交集、差集运算，并返回运算结果的集合。

.. attention::
    * 此模块并不会对原集合进行修改。
    * 并集是将两个集合的元素在删除重复元素后组成的集合。
    * 交集是两个集合中都包含的元素组成的集合。
    * 差集是包含在第一个集合中，但不包含在第二个集合(方法的参数)中的元素组成的集合。

4.2 示例
++++

    .. image:: images/09Set/set_operator_example.png

    源代码：

    .. code-block:: python
            :linenos:

            s1= {"Mixly", 1, "Mixpy", 2}
            s2= {"Mixly", 2, "MixGo", 3}
            print(s1)
            print(s2)
            print(s1.union(s2))
            print(s1.intersection(s2))
            print(s1.difference(s2))
            print(s1)
            print(s2)

    输出结果：

    .. code-block:: python

        {'Mixly', 1, 2, 'Mixpy'}
        {'Mixly', 2, 3, 'MixGo'}
        {1, 2, 3, 'MixGo', 'Mixly', 'Mixpy'}
        {'Mixly', 2}
        {1, 'Mixpy'}
        {'Mixly', 1, 2, 'Mixpy'}        # 此模块并不会对原集合进行修改
        {'Mixly', 2, 3, 'MixGo'}

5.集合运算并更新
-----------------

.. image:: images/09Set/set_operator_refresh.png
5.1 描述
++++
    此模块可以将给定的两个集合进行并集、交集、差集运算，并将运算结果的集合赋值给第一个参数集合。

.. attention::
    * 此模块没有返回值。
    * 此模块只会改变第一个参数集合，不会改变第二个参数集合。

5.2 示例1
++++

    .. image:: images/09Set/set_operator_refresh_example.png

    源代码：

    .. code-block:: python
            :linenos:

            s1= {"Mixly", 1, "Mixpy", 2}
            s2= {"Mixly", 2, "MixGo", 3}
            print(s1)
            print(s2)
            s1.update(s2)
            print(s1)
            print(s2)

    输出结果：

    .. code-block:: python

        {'Mixpy', 1, 'Mixly', 2}
        {3, 'MixGo', 'Mixly', 2}
        {1, 2, 3, 'MixGo', 'Mixly', 'Mixpy'}        # 更新后的第一个参数集合
        {3, 'MixGo', 'Mixly', 2}

5.3 示例2
++++

    .. image:: images/09Set/set_operator_refresh_example2.png

    源代码：

    .. code-block:: python
            :linenos:

        s1= {"Mixly", 1, "Mixpy", 2}
        s2= {"Mixly", 2, "MixGo", 3}
        print(s1)
        print(s2)
        s1.intersection_update(s2)
        print(s1)
        print(s2)

    输出结果：

    .. code-block:: python

        {1, 2, 'Mixpy', 'Mixly'}
        {'MixGo', 2, 3, 'Mixly'}
        {2, 'Mixly'}        # 更新后的第一个参数集合
        {'MixGo', 2, 3, 'Mixly'}

5.4 示例3
++++

    .. image:: images/09Set/set_operator_refresh_example3.png

    源代码：

    .. code-block:: python
            :linenos:

            s1= {"Mixly", 1, "Mixpy", 2}
            s2= {"Mixly", 2, "MixGo", 3}
            print(s1)
            print(s2)
            s1.difference_update(s2)
            print(s1)
            print(s2)

    输出结果：

    .. code-block:: python

        {1, 'Mixpy', 2, 'Mixly'}
        {'MixGo', 2, 3, 'Mixly'}
        {1, 'Mixpy'}        # 更新后的第一个参数集合
        {'MixGo', 2, 3, 'Mixly'}

6.集合增加和删除元素
-----------------

.. image:: images/09Set/set_add_remove.png
6.1 描述
++++
    此模块可以根据所给值在集合中增加或删除元素。

.. attention::
    * Mixly中，此模块仅能够在集合中增加或删除数字元素。
    * 如果添加的元素在集合中已存在，则不执行任何操作。
    * 此模块没有返回值。

6.2 示例
++++

    .. image:: images/09Set/set_add_remove_example.png

    源代码：

    .. code-block:: python
            :linenos:

            s1= {"Mixly", 1, "Mixpy", 2}
            print(s1)
            s1.add(3)
            print(s1)
            s1.add(2)
            print(s1)
            s1.discard(1)
            print(s1)

    输出结果：

    .. code-block:: python

        {'Mixpy', 1, 2, 'Mixly'}
        {'Mixpy', 1, 2, 3, 'Mixly'}     # 添加元素3
        {'Mixpy', 1, 2, 3, 'Mixly'}     # 添加元素2，因为集合中已包含元素2，所以没有执行任何操作
        {'Mixpy', 2, 3, 'Mixly'}        # 删除元素1

7.集合拆分增加序列元素
-----------------

.. image:: images/09Set/set_update.png
7.1 描述
++++
    此模块可以将给定的字符串、列表、元组、集合等拆分增加到集合中。

.. attention::
    * 若是字符串，则将会把字符串拆分为一个一个字符作为元素添加进集合中。
    * 在添加时，如果添加的元素在集合中已存在，则该元素只会出现一次，重复的会忽略。
    * 此模块没有返回值。

7.2 示例
++++

    .. image:: images/09Set/set_update_example.png

    源代码：

    .. code-block:: python
            :linenos:

            s1= {"Mixly", 1, "Mixpy", 2}
            print(s1)
            s1.update([1,2,3,4,5])
            print(s1)
            s1.update("MixGo")
            print(s1)
            s1.update((6,7,8,9,0))
            print(s1)
            s1.update({"A","B","C"})
            print(s1)

    输出结果：

    .. code-block:: python

        {1, 2, 'Mixpy', 'Mixly'}
        # 添加列表[1,2,3,4,5]，因为1，2已存在，所以并未添加
        {1, 2, 3, 4, 5, 'Mixpy', 'Mixly'}
        # 添加字符串"MixGo"，字符被拆分后添加到集合里
        {1, 2, 3, 4, 5, 'i', 'x', 'Mixpy', 'Mixly', 'G', 'M', 'o'}
        # 添加元组(6,7,8,9,0)
        {0, 1, 2, 3, 4, 5, 'i', 'x', 6, 7, 8, 9, 'Mixpy', 'Mixly', 'G', 'M', 'o'}
        # 添加集合{"A","B","C"}
        {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'C', 'B', 'Mixpy', 'A', 'x', 'M', 'i', 'Mixly', 'G', 'o'}

8.集合子集超集判断
-----------------

.. image:: images/09Set/set_son_father.png
8.1 描述
++++
    此模块可以判断两个集合的子集和超集关系，并返回判断的逻辑值。

    若前一集合是后一集合的子集或超集，则将返回True；否则将返回False。

8.2 示例
++++

    .. image:: images/09Set/set_son_father.png

    源代码：

    .. code-block:: python
            :linenos:

            s1= {"Mixly", 1, "Mixpy", 2}
            s2= {1, 2}
            s3= {2, 3}
            print(s1)
            print(s2)
            print(s3)
            print(s2.issubset(s1))
            print(s3.issubset(s1))
            print(s1.issuperset(s2))
            print(s1.issuperset(s3))
            print(s2.issubset(s3))
            print(s2.issuperset(s3))

    输出结果：

    .. code-block:: python

        {1, 'Mixly', 2, 'Mixpy'}
        {1, 2}
        {2, 3}
        True        # s2是否是s1的子集
        False       # s3是否是s1的子集
        True        # s1是否是s2的超集
        False       # s1是否是s3的超集
        False       # s2是否是s3的子集
        False       # s2是否是s3的超集
