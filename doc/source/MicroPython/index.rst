MicroPython 编程
================

.. toctree::
   :maxdepth: 1

   01Input-Output.rst
   02Control.rst
   03Mathematics.rst
   04Logic.rst
   05Text.rst
   06Lists.rst
   07Tuple.rst
   08Dictory.rst
   09Set.rst
   10Variables.rst
   11Functions.rst
   12Serial.rst
   13Sensor.rst
   14Actuator.rst
   15Monitor.rst
   16File.rst
   17Communicate.rst
   18Network.rst
   19IOT.rst
   20Factory.rst
   Handbit.rst
   MixGo.rst
