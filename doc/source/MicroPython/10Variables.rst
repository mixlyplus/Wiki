变量
==============
.. note::
    变量是存储在内存中的值，变量可以指定不同的数据类型，存储各种不同类型的数据。

    Python中，有五种标准的数据类型：Numbers（数字），String（字符串），List（列表），Tuple（元组），Dictionary（字典）。

    Numbers（数字）又包含四种类型：int（有符号整型），long（长整型[也可以代表八进制和十六进制]），float（浮点型），complex（复数）。

    Mixly中有很多用于变量操作的模块，如下：

.. image:: images/10Variables/variables.png

.. hint::
    当你使用其他模块创建变量之后，变量分类下会增加为此变量赋值和调用此变量的模块。

    .. image:: images/10Variables/variables_example.png

1.使用全局变量
-----------------

.. image:: images/10Variables/global.png
1.1 描述
++++
    Python中定义函数时，若想在函数内部对函数外的变量进行操作，就需要声明其为global（全局变量）。

.. hint::
    全局变量实际上是为了提示 python 解释器，表明被其修饰的变量是全局变量。这样解释器就可以从当前空间中读写相应变量了。

    Python 的全局变量是模块 (module) 级别的

    每个 python 函数拥有对应的 __globals__ 字典，该字典与函数所属模块的 __dict__ 字典完全相同。函数的全局变量也会从这个字典中获取

    即：python 解释器发现函数中的某个变量被 global （全局变量）关键字修饰，就去函数的 __globals__ 字典变量中寻找（因为 python 中函数也是一等对象）；同时，一个模块中每个函数的 __globals__ 字典变量都是模块 __dict__ 字典变量的引用，二者值完全相同。

1.2 返回值
++++

.. attention::
    此模块没有返回值。

1.3 示例1
++++

    .. image:: images/10Variables/global_example.png

    源代码：

    .. code-block:: python
            :linenos:

            x = 1
            def function1():
                x = 2
            function1()
            print(x)

    输出结果：

    .. code-block:: python

        1   # 在函数中并未使用全局变量，所以函数无法将x赋为2，无法改变x的值

1.4 示例2
++++

    .. image:: images/10Variables/global_example2.png

    源代码：

    .. code-block:: python
            :linenos:

            x = 1
            def function1():
                global x
                x = 2
            function1()
            print(x)

    输出结果：

    .. code-block:: python

        2   # 在函数中使用全局变量，函数将x赋为2

1.5 示例3
++++

    .. image:: images/10Variables/global_example3.png

    源代码：

    .. code-block:: python
            :linenos:

            global x
            x = 1
            def function1():
                x = 2
            function1()
            print(x)

    输出结果：

    .. code-block:: python

        1   # 使用全局变量需要在函数内部声明，在外部声明函数依然无法将x赋为2

2.初始化变量
-----------------

.. image:: images/10Variables/variable_initial.png
2.1 描述
++++
    此模块对变量进行初始化，或者为其赋值。

    你需要在模块中输入变量名称，如果不存在此变量，则将会创建此变量并赋值；如果存在此变量，则将会为此变量赋新值。

    使用此模块创建变量之后，变量分类下会增加为此变量赋值和调用此变量的模块。

2.2 返回值
++++

.. attention::
    此模块没有返回值。

2.3 示例
++++

    .. image:: images/10Variables/variable_initial_example.png

    源代码：

    .. code-block:: python
            :linenos:

            x = 1
            mytext = "Mixly"
            mylist = [1,2,3,4,5]
            mytup = (1,2,3,4,5)
            myset = {1,2,3,4,5}


    输出结果：

    .. code-block:: python

        # 没有输出结果

3.数据类型转换
-----------------
.. image:: images/10Variables/variable_transform.png

.. note::
    在Python中，各种类型的数据变量可以灵活便捷地进行转换。

    但需要注意的是，数据类型转换需要建立在有意义的基础上，你不能够在非数字字符串和数字类型的变量之间转换等等。

3.1 转换为整数
++++
3.1.1 描述
****
    你可以将小数（float）、布尔值和字符串转换为整数。

.. attention::
    * 将小数转换为整数时，将会直接取小数的整数部分；
    * 布尔值转换为整数，True将转换为1，False将转换为0；
    * 字符串转换为整数时，字符串的内容必须是整数，字符串类型的小数无法转换为整数，会触发报错。

3.1.2 示例
****

    .. image:: images/10Variables/variable_transform_int_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(int('7'))
            print(int(6.6))
            print(int(7.4))
            print(int(True))
            print(int(False))

    输出结果：

    .. code-block:: python

        7   # '7'
        6   # 6.6
        7   # 7.4
        1   # True
        0   # False

3.2 转换为小数
++++
3.2.1 描述
****
    你可以将整数、布尔值和字符串转换为小数。

.. attention::
    * 将整数转换为小数时，将会直接在整数后面添加小数点和0；
    * 布尔值转换为小数，True将转换为1.0，False将转换为0.0；
    * 字符串转换为小数时，字符串的内容可以是整数也可以是小数，字符串类型的整数转换为小数时将会在整数后添加小数点和0。

3.2.2 示例
****

    .. image:: images/10Variables/variable_transform_float_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(float('7'))
            print(float('7.3'))
            print(float(6))
            print(float(True))
            print(float(False))

    输出结果：

    .. code-block:: python

        7.0     # '7'
        7.3     # '7.3'
        6.0     # 6
        1.0     # True
        0.0     # False

3.3 转换为布尔值
++++
3.3.1 描述
****
    你可以将所有类型的变量转换为布尔值。

.. attention::
    * 将所有非空序列（列表、元组、字典、集合、字符串）转换为布尔值时，都会转换为True，空序列将转换为False；
    * 将整数和小数转换为布尔值时，1和1.0将转换为True，0和0.0将转换为False；
    * 空对象（None）转换为布尔值为False；

3.3.2 示例
****

    .. image:: images/10Variables/variable_transform_bool_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(bool("7.3"))
            print(bool("0"))
            print(bool(""))
            print(bool(6))
            print(bool(0))
            print(bool(None))
            print(bool([1,2,3,4,5]))
            print(bool((1,2,3,4,5)))
            print(bool({'Name': 'Tom', 'Age': 18}))
            print(bool({1,2,3,4,5}))

    输出结果：

    .. code-block:: python

        True    # "7.3"
        True    # "0"
        False   # ""
        True    # 6
        False   # 0
        False   # None
        True    # [1,2,3,4,5]
        True    # (1,2,3,4,5)
        True    # {'Name': 'Tom', 'Age': 18}
        True    # {1,2,3,4,5}

3.4 转换为字符串
++++
3.4.1 描述
****
    你可以将所有类型的变量转换为字符串。

3.4.2 示例
****

    .. image:: images/10Variables/variable_transform_str_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(str(6))
            print(str(7.3))
            print(str(False))
            print(str(None))
            print(str([1,2,3,4,5]))
            print(str((1,2,3,4,5)))
            print(str({'Name': 'Tom', 'Age': 18}))
            print(str({1,2,3,4,5}))

    输出结果：

    .. code-block:: python

        # 以下输出的变量类型都是str，可以使用type()方法查看
        6
        7.3
        False
        None
        [1, 2, 3, 4, 5]
        (1, 2, 3, 4, 5)
        {'Name': 'Tom', 'Age': 18}
        {1, 2, 3, 4, 5}

3.5 转换为非字典序列
++++
3.5.1 描述
****
    非字典序列类型（列表、元组、集合、字符串）变量可以相互转换，字典也可以转换为非字典序列类型。

    数字、布尔值等类型的变量无法转换为序列类型的变量。

.. attention::
    将字典转换为其他非字典序列类型时，会将字典中的键作为序列的元素添加到序列中。

3.5.2 示例
****

    .. image:: images/10Variables/variable_transform_line_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(list("Mixly"))
            print(list((1,2,3,4,5)))
            print(list({'Name':"Tom", 'Age':18, 'Country':"China"}))
            print(list({1,2,3,4,5}))

            print(tuple("Mixly"))
            print(tuple([1,2,3,4,5]))
            print(tuple({'Name':"Tom", 'Age':18, 'Country':"China"}))
            print(tuple({1,2,3,4,5}))

            print(set("Mixly"))
            print(set([1,2,3,4,5]))
            print(set((1,2,3,4,5)))
            print(set({'Name':"Tom", 'Age':18, 'Country':"China"}))

    输出结果：

    .. code-block:: python

        ['M', 'i', 'x', 'l', 'y']
        [1, 2, 3, 4, 5]
        ['Name', 'Age', 'Country']
        [1, 2, 3, 4, 5]
        ('M', 'i', 'x', 'l', 'y')
        (1, 2, 3, 4, 5)
        ('Name', 'Age', 'Country')
        (1, 2, 3, 4, 5)
        {'l', 'x', 'y', 'M', 'i'}
        {1, 2, 3, 4, 5}
        {1, 2, 3, 4, 5}
        {'Name', 'Country', 'Age'}      # 集合是无序的

3.6 转换为字典
++++
3.6.1 描述
****
    你可以将元素为二元序列的序列转换为字典，转换后，每个二元序列的第一个元素为字典的键，第二个元素为对应的键值。

.. attention::
    你可以使用 :ref:`list_zip` 的方式将两个序列打包成为元素为二元序列的序列。

3.6.2 示例
****

    .. image:: images/10Variables/variable_transform_dict_example.png

    源代码：

    .. code-block:: python
            :linenos:

            keys = ["Name", "Age", "Country"]
            values = ["Fred", "19", "China"]
            print(dict(zip(keys, values)))

    输出结果：

    .. code-block:: python

        {'Name': 'Fred', 'Age': '19', 'Country': 'China'}

4.获取变量数据类型
-----------------

.. image:: images/10Variables/variable_get_type.png
4.1 描述
++++
    此模块可以获取各种变量的数据类型并返回。

    你可以使用此模块获取一些模块返回的未知的对象的数据类型。

4.2 示例
++++

    .. image:: images/10Variables/variable_get_type_example.png

    源代码：

    .. code-block:: python
            :linenos:

            print(type(7))
            print(type(7.3))
            print(type("Mixly"))
            print(type([1,2,3,4,5]))
            print(type((1,2,3,4,5)))
            print(type(dict(zip(["Name", "Age", "Country"], ["Fred", "19", "China"]))))
            print(type({1,2,3,4,5}))
            print(type(False))
            print(type(None))
            print(type(zip(["Name", "Age", "Country"], ["Fred", "19", "China"])))
            print(type(dict(zip(["Name", "Age", "Country"], ["Fred", "19", "China"])).keys()))
            print(type(dict(zip(["Name", "Age", "Country"], ["Fred", "19", "China"])).items()))
            print(type(dict(zip(["Name", "Age", "Country"], ["Fred", "19", "China"])).values()))

    输出结果：

    .. code-block:: python

        <class 'int'>
        <class 'float'>
        <class 'str'>
        <class 'list'>
        <class 'tuple'>
        <class 'dict'>
        <class 'set'>
        <class 'bool'>
        <class 'NoneType'>      # None的数据类型
        <class 'zip'>           # 迭代打包返回的数据类型
        <class 'dict_keys'>     # 获取字典所有键的模块返回的数据类型
        <class 'dict_items'>    # 字典转换为列表的模块返回的数据类型
        <class 'dict_values'>   # 获取字典所有键值的模块返回的数据类型

5.变量类型模块
-----------------

.. image:: images/10Variables/variable_type.png
5.1 描述
++++
    此模块用于在变量类型判断中作为代表各种变量类型的模块。

5.2 示例
++++

    在第一个条件语句中，若变量a是字符串类型，则打印输出a，否则输出"变量a不是字符串类型。"；在第二个条件语句中，若变量a是整数类型，则打印输出a，否则输出"变量a不是整数类型。"

    .. image:: images/10Variables/variable_type_example.png

    源代码：

    .. code-block:: python
            :linenos:

            a = "Mixly"
            if type(a) == str:
                print(a)
            else:
                print("变量a不是字符串类型。")
            if type(a) == int:
                print(a)
            else:
                print("变量a不是整数类型。")

    输出结果：

    .. code-block:: python

        Mixly
        变量a不是整数类型。
